/*
  Dieses Programm stellt die ASCII-Code-Tabelle dar

  Sonderzeichen lassen sich durch dr�cken von
  ALT und einer Ziffernkolone auf dem Num-Block darstellen.

  Probiere "ALT+64" !
*/
#include <stdio.h>
#include <conio.h>

void main()
{
  unsigned char c;
  int h=0;

  clrscr();
  printf("ASCII-Code Tabelle: (#Ziffer ASCII-Zeichen)\n\n");
  for (c=0;c<255;c++)
  {
    if ((c%20==0)&&((int)c)) gotoxy(h++*6,3);
    gotoxy(h*6,(c%20)+3);
    printf("%i %c\n",int(c),c);
  }
  gotoxy(8,24);
  printf("Fertig! Dr�cke beliebige Taste.");
  getch();
}
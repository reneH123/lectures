#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <dos.h>

// zeichnet Farbenband
void farbstreifen()
{
  int i,colors;
  colors=getmaxcolor();
  for (i=0;i<getmaxx();i=i+5){
    setcolor(i);
    rectangle(i,0,i+4,480);
  }
}

// vertauscht periodisch alle PaletteneintrĄge miteinander
void palettecycle()
{
  char c;
  unsigned char i;
  signed char col;
  struct palettetype pal;

  getpalette(&pal);  // speichere aktuelle Farbpalette
  while (!kbhit()){
    for (i=0;i<pal.size;i++){
      col=pal.colors[i+1];
      pal.colors[i+1]=pal.colors[i];
      pal.colors[i]=col;
      delay(80);
    }
    setallpalette(&pal);
  }
}

int main(void)
{
   int gdriver=DETECT,gmode,errorcode;
   initgraph(&gdriver, &gmode, "");
   errorcode = graphresult();
   if (errorcode != grOk){
      printf("Fehler bei Grafikinitialisierung: %s\n", grapherrormsg(errorcode));
      getch();
      exit(-1);
   }
   cleardevice();
   farbstreifen();
   setbkcolor(WHITE);
   setpalette(WHITE,BLACK);
   setcolor(WHITE);
   outtextxy(100,200,"Grafiktest");
   setcolor(RED);
   rectangle(300,100,500,200);
   setfillstyle(SOLID_FILL,RED);
   floodfill(380,180,RED);
   palettecycle();
   getch();
   closegraph();
   return 0;
}
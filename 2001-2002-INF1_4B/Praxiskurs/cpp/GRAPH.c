#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define min(x,y) (x<y)?x:y
#define ECKEN 7
#define INF 65535

typedef unsigned int myscalar;
typedef myscalar vec[ECKEN];

class my_vector
{
public:
	my_vector()					// Initialisator1 ohne Parameter
	{
		memset(s,INF,ECKEN);
	}
	my_vector(const vec &set)	// Initialisator2 mit Eingabeparametern
	{
		int i=0;
		for (;i<ECKEN;i++)
			s[i]=set[i];
	}
	vec s;
};

typedef my_vector matrix[ECKEN];

const my_vector operator+(const my_vector &v1, const my_vector &v2)  // Minimumsoperator
{
  my_vector* s=new my_vector;
  for (int i=0;i<ECKEN;i++)
	  s->s[i]=min(v1.s[i],v2.s[i]);
  return *s;
}

const my_vector operator*(const myscalar scalar, const my_vector &v)  // Additionsoperator
{
  my_vector* s=new my_vector;
  for (int i=0;i<ECKEN;i++)
	  s->s[i]=scalar+v.s[i];
  return *s;
}

void matrixprintf(const matrix &m)
{
	int i,j;
	for (j=0;j<ECKEN;j++)
	{
		printf("\n");
		for (i=0;i<ECKEN;i++)
			(m[i].s[j]==INF)?printf("L "):printf("%d ",m[i].s[j]);
	}
}

int main()
{
   vec vset={1,2,3,4,5,6,7},
	   v1={INF,INF,INF,INF,  3,  1,INF},
       v2={INF,INF,  2,  5,  1,INF,INF},
       v3={INF,  2,INF,  4,INF,INF,INF},
       v4={INF,INF,INF,INF,INF,INF,INF},
       v5={  2,INF,INF,  2,INF,INF,INF},
       v6={  4,INF,INF,  1,INF,INF,INF},
       v7={INF,INF,INF,  1,INF,INF,INF};
	my_vector v(vset);
    matrix e={v1,v2,v3,v4,v5,v6,v7};
    
    int i,j;
    printf("Graphenmodell zur Ermittlung der Entfernungen zwischen \n");
    printf("gegebenen Ecken");
    // Initialisierung
    // Algorithmus zur Optimierung der Entfernungen
    for (j=1;j<ECKEN;j++)
      for (i=1;i<ECKEN;i++)
	  {
		    if ((i!=j)&&(e[i].s[j]!=INF))		  
		      e[i]=e[i]+(e[i].s[j]*e[j]);
	  }
    // Ausgabe:
    printf("\nAusgabe: des fertig-optimierten Graphen:\n\n");
	matrixprintf(e);
	printf("Bitte Taste dr�cken.");
	while (!kbhit()) ;
    return 0;
}
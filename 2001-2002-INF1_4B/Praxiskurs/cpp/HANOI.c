#include <stdio.h>


/*

 der Algorithmus zur L�sung des

 Problems "Turms von Hanoi" kann

 relativ einfach mittels Rekursion

 erledigt werden.

 In diesem Fall beschr�nkt sich

 das eigentliche L�sen auf eine

 rekursive Beschreibung.

*/

void transportiere(int n, char AS, char HS, char ZS)

{

  if (n>1) transportiere(n-1,AS,ZS,HS);

  printf("Lege Scheibe von %c nach %c\n",AS,ZS);

  if (n>1) transportiere(n-1,HS,AS,ZS);

}



void main()

{

  int scheiben;

  clrscr();

  printf("Turm von Hanoi\t");

  printf("Wieviele Scheiben=");

  scanf("%i",&scheiben);

  printf("\n\n");

  transportiere(scheiben,'A','B','C');

  printf("fertig. Dr�cke beliebige Taste!");

  getch();

}


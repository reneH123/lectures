#include <conio.h>
#include <stdio.h>

void main()
{
  double x=2,
  e=0.00001,
  dif;

  clrscr();
  do{
    dif=x-1.41421356237;
    dif=((dif<0)?-dif:dif);
    x=0.5*(x+2/x);
    printf("%f\n",x);
  }while (dif>=e);
  getch();
}
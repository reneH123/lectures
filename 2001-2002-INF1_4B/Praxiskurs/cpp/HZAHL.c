/*
  Aufgabe der hartn�ckigen Zahlen:

  Eine Eingabezahl wird solange als Reihenfolgenprodukt interpretiert, bis
  die Zahl einstellig ist.
  Beispiel:
  772 -> 98 -> 72 -> 14 -> 4

  Umwandlung eines Zeichens in eine Zahl:
    mittels ASCII-Tabelle
    48.Eintrag in Tabelle entspricht dem Zeichen 0
      --> das Zeichen '8' hat also den Wert 56
      --> "         " '�' hat den Wert 133

    Um die Zahl 0 aus dem Zeichen '0' zu erhalten, mu� man folglich
     den ASCII-Eintrag um 48 subtrahieren:

    Zeichen '4' -> Wert: 52 - 48 = 4
*/
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>

void main(void)
{
   int zahl,l,i;  // errechnete Zahl
   char* zk;      // entsprechende Zahl als Zeichenkette

   clrscr();
   printf("Dieses Programm kann Zahlen ""verstehen""\n");
   printf("Ihre Zahl:");
   scanf("%i",&zahl);
   do{
     printf("\n%i",zahl);  // gib jede neu errechnete Zahl aus
     itoa(zahl,zk,10);     // wandle Zahl in Zeichenkette um
     l=strlen(zk);
     zahl=1;
     for (i=0;i<l;i++)     // multipliziere alle auffeianderfolgende Ziffern
	zahl*=zk[i]-48;
   }while(l!=1);           // .. bis einstellige Zahl
   printf("\nFertig. Dr�cke Taste!");
   getch();
}
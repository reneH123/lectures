/*
  Dieses Programm pr�ft, ob eine als Zeichenketten-Ausdruck gegebene ISBN
  korrekt ist.
  Die Internationale Standart Buch Nummer setzt sich wie folgt zusammen:
    a-bcd-efghi-p
    dabei bedeuten:
      a..... Landes-Gruppennummer (3 steht f�r Deutschland/�sterreich/Schweiz)
      bcd... Verlagsnummer
      efghi. Titelnummer des Buches
      p..... Pr�fziffer

  p dient versehentlich falsche Buchungen des Buchverlages zu entdecken

  p setzt sich wie folgt zusammen:

  p= 10*a + 9*b + 8*c +7*d + 6*e + 5*f + 4*g + 3*h + 2*i

  ... und p mu� durch 11 restfrei teilbar sein!
*/
#include <stdio.h>
#include <stdlib.h>

/*
  Benutzung von Programmparametern:

  Aufruf aus DOS-Console: isbn.exe a b c d e f g h i p
  oder aus BCPP:  in "Ausf�hren\Argumente\" Parameter eintragen
*/

void druckenachricht(char** strings,int groesse, int richtigkeit)
{
  int i;
  printf("\nDie ISBN ");
  for (i=1;i<groesse;i++)
    printf("%s",strings[i]);
  if (!richtigkeit) printf(" ist korrekt!\n");
  else printf(" ist fehlerhaft!");
}

void main(int argc,char** argv)
{
  if (argc!=11){
    printf("Fehler in Eingabeparametern:\n ISBN ist 10 stellig\n");
    printf("\tisbn.exe a b c d e f g h i p\n");
    exit(-1);
  }
  int i;
  unsigned int p=0;
  for (i=1;i<argc;i++)
    p+=(11-i)*atoi(argv[i]);
  p%=11;
  druckenachricht(argv,argc,p);
}
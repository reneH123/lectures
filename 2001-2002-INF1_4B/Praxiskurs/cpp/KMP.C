/*
  Suchalgorithmus nach Knuth,Morris,Pratt (KMP-Algorithmus)
*/
#include <string.h>
#include <memory.h>
#include <stdio.h>

#define _N 200 // L�nge des Textes
#define _M 200  // L�nge des Suchwortes

void main()
{
  unsigned char text[]="ABBBCCBAABBBBCCAB";
  unsigned char wort[_M]="BBBBCC";
  unsigned int next[_M];   // Verschiebefeld

  int i,j;
  int m=strlen(wort),
      n=strlen(text);
  printf("\n\nSuch-Algorithmus nach Knuth Morris Pratt:\n\n");
  printf("Suchtext: %s\n",text);
  printf("Suchwort: %s\n",wort);// in\n%s\n\t nach ,wort,text);
  // 1. Berechnung des Verschiebefeldes
  i=1;
  j=0;
  next[1-1]=0;
  do{
    if ((j==0)||(wort[i-1]==wort[j-1])){
      i++;
      j++;
      if (wort[i-1]!=wort[j-1]) next[i-1]=j;
      else next[i-1]=next[j-1];
    }
    else j=next[j-1];
  }while(i<m);
  for (i=0,printf("Verschiebefeld: [");i<m;i++)   // Ausgabe des Verschiebefeldes
    printf("%d,",next[i]);
  gotoxy(wherex()-1,wherey());
  printf("]\n\n");
  // 2. eigentlicher Suchalgorithmus
  i=1;
  j=1;
  do{
    if ((j==0)||(text[i-1]==wort[j-1])){
      i++;
      j++;
    }
    else j=next[j-1];
  }while((j<=m)&&(i<=n));
  if (j>m) printf("1.Auftreten im Text an Position %d\n",i-m);
  else printf("Keine �bereinstimmung mit Wort!");
  return;
}
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#define WIDTH 10
#define HEIGHT 10

typedef unsigned char Byte;
typedef struct Coords{
  Byte x,y;
};
enum Boolean {false=0,true=1};

int laby[WIDTH*HEIGHT] =
 {0,0,0,0,0,0,0,0,0,0,  // 0.. gesperrt
  0,0,0,1,1,1,1,1,1,0,  // 1.. Weg
  0,1,1,1,0,1,0,0,1,0,  // 2.. Ziel
  0,0,0,0,1,1,1,0,1,0,  // 3.. Anfang
  1,1,1,0,0,1,0,1,1,0,
  0,0,1,0,0,1,0,0,1,0,
  0,0,1,0,1,1,1,0,1,0,
  0,0,1,0,1,0,0,0,1,0,
  0,0,1,1,1,1,1,0,2,0,
  0,0,0,0,0,0,0,0,0,0};

void darstellen()
{
   int i,swcol;
   for (i=0;i<(WIDTH*HEIGHT);i++)
   {
     switch (laby[i])
     {
       case 0: swcol=RED;   break;
       case 1: swcol=CYAN;  break;
       case 2: swcol=BLUE;  break;
       case 3: swcol=GREEN; break;
     }
     textbackground(swcol);
     gotoxy(i%WIDTH,i/WIDTH); // zeile/spalte
     cprintf("  ");
   }
}

void weg(Byte x, Byte y, Boolean* fertig)
{
   if ((*fertig)==0)
   {
     if (laby[y*WIDTH+x]==2) *fertig=true;
     else if ((laby[y*WIDTH+x]!=0)&&(laby[y*WIDTH+x]!=3))
     {
       laby[y*WIDTH+x]=3;
       darstellen();
       gotoxy(x,y);
       textbackground(MAGENTA);
       cprintf("�");
       weg(x+1,y,false);
       weg(x-1,y,false);
       weg(x,y+1,false);
       weg(x,y-1,false);
     }
   }
}

void main()
{
  Coords playerpos;
  playerpos.x=0;
  playerpos.y=4;
  clrscr();
  textbackground(BLACK);
  weg(playerpos.x,
      playerpos.y,
      false);
  printf("Bitte Taste dr�cken!");
  getch();
  textbackground(BLACK);
  textcolor(WHITE);
  clrscr();
}
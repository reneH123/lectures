/*
 Dieses Programm sortiert eine Zahlenkolonne mittels
 MinimumSort: es wird solange das gesamte Feld
	      aufsteigend sortiert, bis innerhalb eines
	      Sortierzyklus keine Vertauschungen mehr
	      notwendig sind
*/
#include <stdio.h>
#include <conio.h>

void main()
{
  const int MAXELEMENTS=20;
  int zahlen[MAXELEMENTS];
  int merker,pos,getauscht;

  /*
   da Initialisierung fehlt, werden die Zahlen im
   Feld "zahlen" zuf�llig eingetragen! Diese Art
   der Zufallszahlengenerierung ist unter allen
   Umst�nden beim Programmieren zu vermeiden!
   Dieser Programmcode soll zeigen, wie man auf
   gar keinen Fall Zufallszahlen erzeugen sollte,
   da man hier auch keine erzeugt!!!
  */
  do{
    getauscht=0;              // 0.. "falsch"
    for (pos=0;pos<MAXELEMENTS;pos++)
      if (zahlen[pos]>zahlen[pos+1]){
	merker=zahlen[pos];   // f�hre Dreieckstausch durch
	zahlen[pos]=zahlen[pos+1];
	zahlen[pos+1]=merker;
	getauscht=1;          // es wurde getauscht: 1.."wahr"
      }
  }while (getauscht);
  /*
    das sortierte Feld kann mittels "Strg+F7"
    �berpr�ft werden

    Da man das Programm u.a. nicht ohne die BorlandC++
    - Umgebung anwenden kann, ist das Programm
    als Demonstrationsobjekt wertlos!
  */
  getch();
}
/*
  Kurs inf1_4b
	 03.01.2002
  innerhalb des Eingabeworts soll jedes Zeichen (dezimaler Wert) um Codezahl
   erh�ht werden

  Eingabewort: "hallo" (ASCII dezimal: 104,97,108,108,111)

  Codezahl:    15

  codiertes
  Ausgabewort: 104+15,104+15,97+15,108+15,108+15,111
	       entspricht: "wp{{}"
*/
#include <conio.h>
#include <stdio.h>
#include <string.h>

/*
  Um Mi�verst�ndinisse zu vermeiden, eignet es sich Werteparameter mit
  dem Schl�sselwort "const" zu versehen. Dadurch wird es nicht m�glich den
  Variableninhalt zu ver�ndern
*/
void verarbeitung(const char* wort,const int cz,char* cwort,int &laenge)
{
  int i;
  for (i=0;i<strlen(wort);i++)
  {
    int h = (int)wort[i];
    h+=cz;
    cwort[i]=h;
  }
  laenge=i;
}

void ausgabe(const char* string, int laenge)
{
  int i;
  printf("Das verschl�sselte Wort lautet:");
  for (i=0;i<laenge;i++)
    printf("%c",string[i]);
  printf("\nFertig! Dr�cke beliebige Taste!");
  getch();
}

void main()
{
   char wort[10], codewort[10];
   int codezahl,lang;

   printf("Gib dein zu verschl�sselndes Wort ein:");
   scanf("%s",&wort);
   printf("Gib dein Verschl�sselungscode ein:");
   scanf("%i",&codezahl);
   verarbeitung(wort,codezahl,codewort,lang);
   ausgabe(codewort,lang);
}

/*
 letzte Bemerkungen: immer noch leichtes Flackern sichtbar,
		     (Synrchronisationsphase unbeachtet!?)
 Dieses Programm demonstriert, wie man
 mittels "Page-Flipping" ein unkontrollierten
 flackernden Bildwechsel vermeidet.
   Als Beispiel Grafik wandern Kreis von rechts
 nach links �ber den Bildschirm

 Im EGA/VGA-Grafikmodus beschreibt man die nicht
 sichtbare Grafikseite, um diese anschlie�end
 mit der Hintergrundseite zu vertauschen - ein
 Bildschirml�schen der nichtaktiven Seite hat
 so keinerlei Konsequenz!
 Achtung: Einige Grafiktreiber unterst�tzen
	  in einigen Modi nur 1 Grafikseite

 setvisualpage(int seite)
 setactivepage(int seite);

Ren� Haberland; Dienstag 19.03.2002; 18:29 Uhr

*/
#include <dos.h>
#include <graphics.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

const MAXELEMENTS=10;
const MAXRADIUS=50;

typedef struct tkreis{
  int x;
  int y;
  int radius;
  int geschwindigkeit;
  //... es k�nnen noch weitere Eigenschaften hinzudefiniert werden
  unsigned char farbe;
};
typedef tkreis tkreise[MAXELEMENTS];

//  Positioniere alle MAXELEMENTS- Kreise zuf�llig an Bildschirmrand
void initallekreise(tkreise& kreise)
{
  for (int i=0;i<MAXELEMENTS;i++)
  {
    kreise[i].radius=random(MAXRADIUS)+2;  // angegebene Zufallsbereich willk�rlich festgelegt!
    kreise[i].x=random(getmaxx()-kreise[i].radius)+kreise[i].radius+1;
    kreise[i].y=random(getmaxy()-kreise[i].radius)+kreise[i].radius+1;
    kreise[i].farbe=random(getmaxcolor());
    kreise[i].geschwindigkeit=random(3)+1;
  }
}

// zeichne alle "MAXELEMENTS" Kreise
void zeichneallekreise(tkreise kreise)
{
  for (int i=0;i<MAXELEMENTS;i++)
  {
    setcolor(kreise[i].farbe);
    circle(kreise[i].x,kreise[i].y,kreise[i].radius);
  }
}

// verschiebe alle Kreise linksw�rts
void verschiebeallekreise(tkreise& kreise)
{
  for (int i=0;i<MAXELEMENTS;i++)
  {
    /*
      entsprechender Kreis hat linken Rand erreicht
      nun mu� dieser Kreis zuf�llig an rechten Rand positioniert werden
    */
    if (kreise[i].x+kreise[i].radius<=0)
    {
      kreise[i].x=getmaxx()-kreise[i].radius;
      kreise[i].y=random(getmaxy()-kreise[i].radius)+kreise[i].radius+1;
      kreise[i].geschwindigkeit=random(5)+1;
    }
    else kreise[i].x=kreise[i].x-kreise[i].geschwindigkeit;
  }
}

/*
  vertauscht vordere mit hinterer Grafikseite
  dadurch wird eine flackerfreie Darstellung gew�hrleistet
  Ist vom Prinzip her: Dreieckstausch
*/
void flipPage(int& akt, int& pass)
{
  int h;

  h=akt;
  akt=pass;
  pass=h;
}

int main(void)
{
  int gdriver = EGA,
      gmode = EGAHI,
      errorcode;
  int x,y,ht;
  int i;
  int akt_Nummer;  // merkt sich die aktuelle Grafikseite
  int pass_Nummer; // ~ Grafikseite im Hintergrund
  tkreise Kreise;  // MAXELEMENTS Kreise

  initgraph(&gdriver, &gmode, ""); // initialisiere Grafikmodus
  errorcode = graphresult(); // sichere evtl. Fehler
  if (errorcode!=grOk)       // Fehler ist aufgetreten
  {
    printf("Grafikfehler aufgetreten: %s\n", grapherrormsg(errorcode));
    printf("Dr�cke Taste um Programm zu beenden:");
    getch();
    return -1; // liefere -1 ans System zur�ck
  }
  randomize();
  initallekreise(Kreise);
  cleardevice();
  outtextxy(1,1,"Mit PageFlipping; Zum Beenden anschlie�end Taste dr�cken!");
  getch();
  cleardevice();
  akt_Nummer=0;
  pass_Nummer=1;
  do{
    setvisualpage(pass_Nummer);
    setactivepage(akt_Nummer);
    cleardevice(); // Bildschirm l�schen
    zeichneallekreise(Kreise);
    verschiebeallekreise(Kreise);
    // die Nummern von aktueller und passiver Grafikseite werden vertauscht
    flipPage(akt_Nummer,pass_Nummer);
  }while (!(kbhit()));
  return 0;  // alles komplikationsfrei verlaufen!
}
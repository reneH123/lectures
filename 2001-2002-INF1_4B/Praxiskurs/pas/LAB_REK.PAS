(* Finden eines Weges innerhalb eines gegebenen Labyrinths *)
Program Labyrint_rekursiv;
USES Crt;

CONST LabBreite = 10; (* Breite des Labyrinths *)
      LabHoehe  = 10; (* Hoehe ~ *)
TYPE  LabDimension = 1..LabBreite*LabHoehe;
      TKoord = RECORD
        x: Integer;
        y: Integer;
      END;
(* dient dient als Eingabevorgabe*)
CONST LabTabelle: ARRAY[LabDimension] OF Integer =
 (0,1,1,1,1,1,1,0,0,0,
  1,0,1,0,0,0,1,1,1,0,
  1,0,1,1,1,0,1,0,1,0,
  1,0,0,0,1,0,0,0,1,1,
  1,0,1,1,1,1,1,1,0,1,
  1,0,1,0,0,0,0,1,0,1,
  1,0,0,1,1,1,0,1,0,1,
  1,0,0,1,0,1,1,1,0,1,
  1,0,0,1,0,0,0,0,1,1,
  1,1,1,1,0,2,1,1,1,0);
      Koordinaten: TKoord = (x:1;y:2);
VAR Labyrinth: ARRAY[LabDimension] OF Integer;
    gefunden: Boolean;

(* Gib aktuellen Zustand des Labyrinths aus *)
PROCEDURE Darstellen;
 VAR i: Integer;
BEGIN
  FOR i:=1 TO (LabBreite*LabHoehe) DO BEGIN
    CASE Labyrinth[i] OF
      0: TextBackground(Red);
      1: TextBackground(Cyan);
      2: TextBackground(Blue);
      3: TextBackground(Green);
    END;
    GotoXY(i MOD LabBreite,i DIV LabBreite);
    Write('  ');
  END;
END;

(* Finde einen Weg vom Ausgangspunkt zum Zielpunkt mittels Backtracking *)
PROCEDURE Weg(x,y: Byte; VAR fertig: Boolean);
BEGIN
  IF (NOT(fertig)) THEN BEGIN
    IF (Labyrinth[(y-1)*LabBreite+x]=2) THEN fertig:=TRUE
    ELSE IF ((Labyrinth[(y-1)*LabBreite+x]<>0)AND(Labyrinth[(y-1)*LabBreite+x]<>3)) THEN BEGIN
      Labyrinth[(y-1)*LabBreite+x]:=3;
      Darstellen;
      GotoXY(x,y);
      TextBackground(MAGENTA);
      Write('J');
      Weg(x+1,y  ,fertig);
      Weg(x-1,y  ,fertig);
      Weg(x  ,y+1,fertig);
      Weg(x  ,y-1,fertig);
    END;
  END;
END;

PROCEDURE Init;
 VAR i: Integer;
BEGIN
  FOR i:=1 TO 100 DO
    Labyrinth[i]:=LabTabelle[i];
  gefunden:=FALSE;
  ClrScr;
  TextBackground(Black);
END;

(* Hauptprogramm *)
BEGIN
  Init;
  Weg(Koordinaten.x,Koordinaten.y,gefunden);
  IF (NOT(gefunden)) THEN WriteLn('Es f�hrt kein Weg zum Ziel!');
  WriteLn('Bitte dr�cken Sie eine Taste!');
  ReadKey;
  NormVideo;
END.
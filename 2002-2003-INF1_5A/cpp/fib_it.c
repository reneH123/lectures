 
int fib(int z)
{ int i,p,q,r;
  if (z==0) return 1;
  if (z==1) return 1;
  p=1; q=1;
  for (i=1; i<z ; i++) 
      { r=p+q; p=q; q=r;}
  return r;
}


int fib1(int z)
{ if (z==0) return 1;
  if (z==1) return 1;
  return (fib(z-1) + fib(z-2));
}
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define FNAME "zahlen.txt"
#define ZAHL2 16016309
#define ZAHL 19522571

int main()
{
	FILE *f;
	unsigned long buf,i,limit=(unsigned long)sqrt(ZAHL); // f�r 5 stellige Zahlen

	f=fopen(FNAME,"r");
	if (f==NULL)
	{
		printf("Fehler beim �ffnen von %s!\nProgramm beendet!",FNAME);
		return 0;
	}
	for (i=0;i<limit;i++)
	{
		fscanf(f,"%u",&buf);
		if (ZAHL%buf==0)
		{
			printf("%u = %u x %u\n",ZAHL,buf,ZAHL/buf);
			i=limit;
		}
	}
	fclose(f);
	return 1;
}
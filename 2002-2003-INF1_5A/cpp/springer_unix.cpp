/*
Springer-Problem:
Gesucht ist eine Zugfolge so, da� jedes Schachfeld vom Springer
genau einmal besucht wird. Es m�ssen alle Felder besucht sein
*/
#include <stdio.h>
#include <stdlib.h>

#define n 5 // Schachfeld = 8x8
int brett[n][n];
int i,j;
long int cnt;

void druckeBrett()
{
  printf("\nVariante %u\n",++cnt);
  int i,j;
  for (i=0;i<n;i++)
  {
    for (j=0;j<n;j++)
    {
      if (brett[i][j]) printf("%i",brett[i][j]);
      printf("\t");
    }
    printf("\n");
  }
}

void springe(int x, int y, int zugnr)
{
  brett[x][y]=zugnr;
  if (zugnr==n*n) druckeBrett();
  else
  {
  /*                   x

*****1*2*******        -
****3***4******               -   y  +
******S********
****5***7******        +
*****6*8*******

  */
    if ((x-1>=0)&&(y-2>=0)&&(brett[x-1][y-2]==0)) springe(x-1,y-2,zugnr+1); //1
    if ((x+1 <n)&&(y-2>=0)&&(brett[x+1][y-2]==0)) springe(x+1,y-2,zugnr+1); // 2
    if ((x-2>=0)&&(y-1>=0)&&(brett[x-2][y-1]==0)) springe(x-2,y-1,zugnr+1); // 3
    if ((x+2 <n)&&(y-1>=0)&&(brett[x+2][y-1]==0)) springe(x+2,y-1,zugnr+1); // 4
    if ((x-2>=0)&&(y+1 <n)&&(brett[x-2][y+1]==0)) springe(x-2,y+1,zugnr+1); // 5
    if ((x-1>=0)&&(y+2 <n)&&(brett[x-1][y+2]==0)) springe(x-1,y+2,zugnr+1); // 6
    if ((x+2 <n)&&(y+1 <n)&&(brett[x+2][y+1]==0)) springe(x+2,y+1,zugnr+1); // 7
    if ((x+1 <n)&&(y+2 <n)&&(brett[x+1][y+2]==0)) springe(x+1,y+2,zugnr+1); // 8
  }
  brett[x][y]=0;
}


void main()
{
  printf("Demonstration des Springer-Problems:\n\n");
  printf("Es soll eine Folge des Schach-Springers gefunden");
  printf("werden, bei der jedes Feld genau einmal besucht wird.\n\n");
  for (i=0;i<n;i++)
    for (j=0;j<n;j++)
      brett[i][j]=0;
  springe(0,0,1);  // Anfangsposition brett[3,3]
}

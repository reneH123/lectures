/*
 2.12.2003
  Sch�ler-Datenbank:

  M�gliche Aktionen:
    - alle Sch�ler ausgeben
    - Informationen zu einem Sch�ler ausgeben
    - Informationen eines Sch�lers ver�ndern

  zeigt: - die Verwendung von Dateioperationen:{fopen,fwrite,fread,fclose}
	 - wie das modulare Programmierkonzept funktioniert
*/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXELEMENTS 10
#define ALLESANZEIGEN 0
#define ANZEIGEN 1
#define AENDERN 2
#define BEENDEN 3

#define fname "test.dat"

typedef char String50[50];
typedef char String40[40];
typedef unsigned char Byte;

typedef struct tschueler{
  String50 name;
  String40 vorname;
  String50 schule;
  Byte klasse;
  Byte alter;
};

/*
  liest alle Sch�ler aus Datei nach 'schueler'

  statt schueler[..] zu benutzen, besser *schueler
    verwenden, da so deutlich effizienter (nur Adresse)
    -> ist aber KEIN sauberer Programmierstil!!!

*/
void alleslesen(char *dateiname,tschueler *schueler)
{
  FILE *f;
  int geleseneSchueler=0;

  f=fopen(dateiname,"ab");
  if (f==NULL)
  {
    printf("Fehler beim �ffnen der Datei %s",fname);
    exit(0);
  }
  printf("Datei %s erfolgreich ge�ffnet!\n\n",fname);
  printf("Lesen Sch�lerdatens�tze aus Datenbank....");
  while ((feof(f)!=NULL)&&(geleseneSchueler<MAXELEMENTS))
  {
     fread(&schueler[geleseneSchueler],sizeof(tschueler),1,f);
     geleseneSchueler++;
  }
  printf("\nalles erfolgreich gelesen (%d Datens�tze)",geleseneSchueler);
  fclose(f);
}

/*
  schreibt alle Sch�ler aus 'schueler' in Datei

  *schueler ist hier aber zwingend erforderlich,
   da ausdr�cklich eine Ver�nderung bezweckt werden soll
*/
void allesschreiben(char *dateiname,tschueler *schueler)
{
  FILE *f;
  int geschrieben=0;

  f=fopen(dateiname,"ab");
  if (f==NULL)
  {
    printf("Fehler beim �ffnen der Datei %s",fname);
    exit(0);
  }
  printf("Datei %s erfolgreich ge�ffnet!\n\n",fname);
  printf("Schreibe Sch�lerdatens�tze in Datenbank....");
  for (geschrieben=0;geschrieben<MAXELEMENTS;geschrieben++)
   fwrite(&schueler[geschrieben],sizeof(tschueler),1,f);
  printf("\nalles erfolgreich zur�ckgeschrieben (%d)Datens�tze",geschrieben+1);
  fclose(f);
}

/*
  zeichnet alle Sch�ler aus 'schueler' in eine "angenehme" Tabelle
   auf dem Bildschirm
*/
void allesanzeigen(tschueler *schueler)
{
  int aktuellerIndex;
  tschueler aktuellerSchueler;

  printf("Name\tVorname\tSchule\t\Klasse\tAlter\n\n");
  for (aktuellerIndex=0;aktuellerIndex<MAXELEMENTS;aktuellerIndex++)
  {
    aktuellerSchueler=schueler[aktuellerIndex];
    printf("|%s\t|%s\t|%s\t|%d\t|%d\t|\n",
	   aktuellerSchueler.name,
	   aktuellerSchueler.vorname,
	   aktuellerSchueler.schule,
	   aktuellerSchueler.klasse,
	   aktuellerSchueler.alter);
  }
  printf("\n\n");
}

/*
  gibt alle Infos zu einem Sch�ler auf Bildschirm aus
*/
void anzeigen(int nummer,tschueler* schueler)
{
  printf("Sch�lereintrag %d:\n\n",nummer);
  printf("==================\n\n");
  printf("\t Name:%s\n",schueler[nummer].name);
  printf("\t Vorname:%s\n",schueler[nummer].vorname);
  printf("\t Schule:%s\n",schueler[nummer].schule);
  printf("\t Klassenstufe:%d\n",schueler[nummer].klasse);
  printf("\t Alter:%d\n\n",schueler[nummer].alter);
}

/*
  �ndert den Eintrag des 'schulernummer'.-ten Sch�lereintrags in 'schueler'
*/
void aendern(int nummer,tschueler *schueler)
{
  printf("Sch�lereintrag %d:\n\n",nummer);
  printf("==================\n\n");
  printf("neuer Name:");
  scanf("%s",schueler[nummer].name);
  printf("\nneuer Vorname:");
  scanf("%s",schueler[nummer].vorname);
  printf("\nneue Schule:");
  scanf("%s",schueler[nummer].schule);
  printf("\nneue Klassenstufe:");
  scanf("%d",schueler[nummer].klasse);
  printf("\nneues Alter:");
  scanf("%d",schueler[nummer].alter);
}

void main()
{
  Byte auswahl,schuelernummer;
  tschueler schueler[MAXELEMENTS];

  clrscr();
  alleslesen(fname,schueler);
  printf("alle Daten erfolgreich eingelesen!");
  do{
    clrscr();
    printf("W�hlen Sie eine Aktion (nur Zahl):\n");
    printf("\t\tung�ltige Aktionen wirken wie Aktion (%d)\n",BEENDEN);
    printf("(%d) alle Sch�ler anzeigen\n",ALLESANZEIGEN);
    printf("(%d) bestimmten Sch�ler anzeigen\n",ANZEIGEN);
    printf("(%d) bestimmten Sch�ler ""bearbeiten""\n",AENDERN);
    printf("(%d) Programm beenden\n",BEENDEN);
    printf("Ihre Wahl:");
    scanf("%d",&auswahl);
    switch (auswahl)
    {
      case ALLESANZEIGEN: {
			    allesanzeigen(schueler);
			    printf("\n\n\nBitte Taste dr�cken!");
			    getch();
			  }break;
      case ANZEIGEN: {
		       printf("ANZEIGEN:\n========\n");
		       printf("Bitten geben Sie die Sch�ler-Nummer zwischen 0 und %d ein\n",MAXELEMENTS-1);
		       printf("(unzul�ssige Werte werden auf 0 gesetzt):");
		       scanf("%d",&schuelernummer);
		       if ((schuelernummer<0)||(schuelernummer>=MAXELEMENTS))
		       {
			 printf("\tunzul�ssige Sch�lernummer: %d!\n",schuelernummer);
			 schuelernummer=0;
			 printf("Sch�lernummer 0 wurde angenommen!");
		       }
		       anzeigen(schuelernummer,schueler);
		       printf("\n\n\nBitte Taste dr�cken!");
		       getch();
		     }break;
      case AENDERN: {
		       printf("�NDERUNG:\n========");
		       printf("Bitten geben Sie die Sch�ler-Nummer zwischen 0 und %d ein\n",MAXELEMENTS-1);
		       printf("(unzul�ssige Werte werden auf 0 gesetzt)");
		       scanf("%d",&schuelernummer);
		       if ((schuelernummer<0)||(schuelernummer>=MAXELEMENTS))
		       {
			 printf("\tunzul�ssige Sch�lernummer: %d!\n",schuelernummer);
			 schuelernummer=0;
			 printf("Sch�lernummer 0 wurde angenommen!");
		       }
		       aendern(schuelernummer,schueler);
		    }break;
      case BEENDEN: printf("Programm wird ordnungsgem�� beendet."); break;
      default: {
		 auswahl=BEENDEN;
		 printf("Programm wird aufgrund unzul�ssiger Eingabe beendet!");
	       }break;
    }
  }while (auswahl!=BEENDEN);
  allesschreiben(fname,schueler);
  printf("alle Daten erfolgreich zur�ckgeschrieben");
}
(*
 07.12.2003 Ren� Haberland

 Dieses Programm verwaltet Sch�lerdaten in einem Feld.

 Zu Beginn werden alle Daten geladen. 
 Zum Ende werden alle bearbeiteten Daten zur�ckgeschrieben.

 Vorteil: nur 2x Dateizugriffe
 Nachteile: - Datenumfang ist festgeschrieben (MAXELEMENTS)
            - andere Daten sind mit der Zeit nicht auffindbar
*)
PROGRAM Schueler1;
USES Crt;

CONST _fname:String='test.dat';
      _MAXELEMENTS=10;
      _ALLESANZEIGEN=0;
      _ANZEIGEN=1;
      _AENDERN=2;
      _BEENDEN=3;
TYPE  tschueler=RECORD
        name: String[50];
        vorname: String[40];
        schule: String[50];
        klasse: Byte;
        alter: Byte;
      END;
      tschulklasse=ARRAY[1.._MAXELEMENTS] OF tschueler;

(*
  liest alle Sch�ler aus Datei nach 'schueler'
  statt schueler[..] zu benutzen, besser VAR schueler
    verwenden, da so deutlich effizienter (nur Adresse)
    -> ist aber KEIN sauberer Programmierstil!!!
*)
PROCEDURE alleslesen(dateiname: String; VAR schueler: tschulklasse);
 VAR f: FILE OF tschueler;
     geleseneSchueler: Integer;
BEGIN
  geleseneSchueler:=0;
  {$I-}
  Assign(f,dateiname);
  Reset(f);
  {$I+}
  IF (IOResult<>0) THEN BEGIN
    WriteLn('Datei ',dateiname,' konnte nicht ge�ffnet werden!');
    Rewrite(f);
    WriteLn('Datei neu erstellt!');
  END
  ELSE BEGIN
    WriteLn('Datei ',dateiname,' erfolgreich ge�ffnet!');
    WriteLn('Lese Sch�lerdatens�tze aus Datenbank...');
    WHILE (NOT(Eof(f))AND(geleseneSchueler<_MAXELEMENTS)) DO
    BEGIN
      Read(f,schueler[geleseneSchueler]);
      Inc(geleseneSchueler);
    END;
    WriteLn('alles erfolgreich gelesen (',geleseneSchueler,')');
  END;
  Close(f);
END;

(*
  schreibt alle Sch�ler aus 'schueler' in Datei

  VAR schueler ist hier aber zwingend erforderlich,
   da ausdr�cklich eine Ver�nderung bezweckt werden soll
*)
PROCEDURE allesschreiben(dateiname: String; VAR schueler: tschulklasse);
 VAR f: FILE OF tschueler;
     geschrieben: Integer;
BEGIN
  geschrieben:=0;
  {$I-}
  Assign(f,dateiname);
  Reset(f);
  {$I+}
  IF (IOResult<>0) THEN BEGIN
    WriteLn('Fehler beim �ffnen der Datei ',dateiname);
    Halt(1);
  END
  ELSE BEGIN
    WriteLn('Datei ',dateiname,' erfolgreich ge�ffnet!');
    WriteLn('Schreibe Sch�lerdatens�tze in Datenbank...');
    FOR geschrieben:=1 TO _MAXELEMENTS DO BEGIN
      Write(f,schueler[geschrieben]);
    END;
    Close(f);
  END;
END;

(*
 zeichnet alle Sch�ler aus 'schueler' in eine "angenehme" Tabelle
  auf dem Bildschirm
*)
PROCEDURE allesanzeigen(VAR schueler: tschulklasse);
 VAR aktuellerIndex: Integer;
     aktuellerSchueler: tschueler;
BEGIN
  WriteLn('Name           Vorname           Schule       Klasse    Alter');
  FOR aktuellerIndex:=1 TO _MAXELEMENTS DO BEGIN
    aktuellerSchueler:=schueler[aktuellerIndex];
    WriteLn(aktuellerSchueler.name,'      ',
            aktuellerSchueler.vorname,'     ',
            aktuellerSchueler.schule,'     ',
            aktuellerSchueler.klasse,'     ',
            aktuellerSchueler.alter,'     ');
  END;
END;

(*
  gibt alle Infos zu einem Sch�ler auf Bildschirm aus
*)
PROCEDURE anzeigen(nummer: Integer; VAR schueler: tschulklasse);
 VAR aktuellerSchueler: tschueler;
BEGIN
  aktuellerSchueler:=schueler[nummer];
  WriteLn('Sch�lereintrag ',nummer);
  WriteLn('==================');
  WriteLn(' Name: ',schueler[nummer].name);
  WriteLn(' Vorname: ',schueler[nummer].vorname);
  WriteLn(' Schule: ',schueler[nummer].schule);
  WriteLn(' Klassenstufe: ',schueler[nummer].klasse);
  WriteLn(' Alter: ',schueler[nummer].alter);
END;

(* �ndert den Eintrag des 'schuelernummer'.ten Sch�lereintrags in 'schueler' *)
PROCEDURE aendern(nummer: Integer; VAR schueler: tschulklasse);
 VAR aktuellerSchueler: tschueler;
BEGIN
  WriteLn('Sch�lereintrag ',nummer);
  WriteLn('================');
  Write(' neuer Name: ');
  ReadLn(aktuellerSchueler.name);
  Write(' neuer Vorname: ');
  ReadLn(aktuellerSchueler.vorname);
  Write(' neue Schule: ');
  ReadLn(aktuellerSchueler.schule);
  Write(' neue Klasse: ');
  ReadLn(aktuellerSchueler.klasse);
  Write(' neues Alter:');
  ReadLn(aktuellerSchueler.alter);
  schueler[nummer]:=aktuellerSchueler;
END;

VAR
auswahl: Byte;
schuelernummer: Byte;
schueler: tschulklasse;

(* Hauptprogramm *)
BEGIN
  ClrScr;
  alleslesen(_fname,schueler);
  WriteLn('alle Daten erfolgreich eingelesen!');
  REPEAT
    ClrScr;
    WriteLn('W�hlen Sie eine Aktion (nur Zahl):');
    WriteLn(' ung�ltige Aktionen wirken wie Aktion ',_BEENDEN);
    WriteLn('(',_ALLESANZEIGEN,') alle Sch�ler anzeigen');
    WriteLn('(',_ANZEIGEN,') bestimmten Sch�ler anzeigen');
    WriteLn('(',_AENDERN,') bestimmten Sch�ler bearbeiten');
    WriteLn('(',_BEENDEN,') Programm beenden');
    Write('Ihre Wahl:');
    ReadLn(auswahl);
    CASE (auswahl) OF
      _ALLESANZEIGEN: BEGIN
                        allesanzeigen(schueler);
                        WriteLn('Enter zum Best�tigen dr�cken!');
                        ReadLn;
                      END;
      _ANZEIGEN: BEGIN
                   WriteLn('ANZEIGEN:');
                   WriteLn('=========');
                   WriteLn('Bitte geben Sie die Sch�ler-Nummer zwischen 1 und ',_MAXELEMENTS,' ein');
                   WriteLn('(unzul�ssige Werte werden auf 1 gesetzt)');
                   ReadLn(schuelernummer);
                   IF ((schuelernummer<=1)OR(schuelernummer>_MAXELEMENTS)) THEN BEGIN
                     WriteLn(' unzul�ssige Sch�lernummer ',schuelernummer);
                     schuelernummer:=1;
                     WriteLn(' Sch�lernummer 1 wurde angenommen!');
                   END;
                   anzeigen(schuelernummer,schueler);
                   WriteLn('Enter zum Best�tigen dr�cken!');
                   ReadLn;
                 END;
      _AENDERN: BEGIN
                  WriteLn('�nderung:');
                  WriteLn('=========');
                  WriteLn('Bitte geben Sie die Sch�ler-Nummer zwischen 1 und ',_MAXELEMENTS,' ein');
                  WriteLn('(unzul�ssige Werte werden auf 1 gesetzt)');
                  ReadLn(schuelernummer);
                  IF ((schuelernummer<=1)OR(schuelernummer>_MAXELEMENTS)) THEN BEGIN
                    WriteLn(' unzul�ssige Sch�lernummer ',schuelernummer);
                    schuelernummer:=1;
                    WriteLn(' Sch�lernummer 1 wurde angenommen!');
                  END;
                  aendern(schuelernummer,schueler);
                END;
      _BEENDEN: BEGIN
                  WriteLn('Programm wird ordnungsgem�� beendet');
                END;
      ELSE BEGIN
        auswahl:=_BEENDEN;
        WriteLn('Programm wird aufgrund unzul�ssiger Eingabe beendet!');
      END;
    END;
  UNTIL (auswahl=_BEENDEN);
  allesschreiben(_fname,schueler);
  WriteLn('alle Daten erfolgreich zur�ckgeschrieben');
END.
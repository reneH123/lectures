(* 
  14.12.2003 Ren� Haberland

  Programm zur Sch�lerverwaltung. 
  M�gliche Aktionen: {allesanzeigen,anzeigen,aendern,hinzufuegen}

  Vorteil: gew�nschte Aktionen werden sofort umgesetzt
  Nachteil: Aktionen sind bei gro�em Datenumfang nicht mehr effektiv

!Hausaufgabe: - Programm schreiben, da� f�r die Sch�lerdatenbank 
                ca. 1000 Test-Sch�ler-Datens�tze in eine extra Datei schreibt.
                 Bedingungen: (a) Vorname und Nachname m�ssen 
				 mit Gro�buchstaben beginnen und
				 d�rfen keine Umlaute haben. Au�erdem
				 nicht l�nger als 30 Zeichen.
			      (b) Sch�ler d�rfen j�ngstens 6 �ltestens 20 sein
			      (c) Klassen von 1..13
Zusatz:       - Ausgabe der Datenbest�nde nach bestimmten Merkmalen 
              (z.B. "Suche alle Sch�ler, die j�nger als 17 sind, 
               aber einen Vornamen mit mindestens 5 Zeichen haben")
              - Sortierung nach Vornamen oder Schule, ...
              - Benutzerdialog verbessern
                 (z.B. beim �ndern: nachfragen, ob wirklich Satz �ndern 
                		    alten Satz mitausgeben
		    ...)
*)
PROGRAM Schueler2;
USES Crt;

CONST _fname:String='test.dat';
      _MAXELEMENTS=10;
      _ALLESANZEIGEN=0;
      _ANZEIGEN=1;
      _AENDERN=2;
      _HINZU=3;
      _BEENDEN=4;
TYPE  tschueler=RECORD
        name: String[50];
        vorname: String[40];
        schule: String[50];
        klasse: Byte;
        alter: Byte;
      END;
VAR auswahl: Byte;
    schuelernummer: Byte;
    F: FILE OF tschueler;

PROCEDURE allesanzeigen;
 VAR aktuellerSchueler: tschueler;
BEGIN
  {I-}
  Assign(F,_fname);
  Reset(F);
  IF (IOResult<>0) THEN BEGIN
    WriteLn('Datei ',_fname,' konnte nicht ge�ffnet werden!');
    Rewrite(f);
    WriteLn('Datei neu erstellt!');
  END
  ELSE BEGIN
    WHILE NOT Eof(F) DO BEGIN
      Read(F,aktuellerSchueler);
      WriteLn('Name: ',aktuellerSchueler.Name,'  Vorname: ',aktuellerSchueler.Vorname,
              'Schule: ',aktuellerSchueler.Schule,' Klasse: ',aktuellerSchueler.Klasse,
              'Alter: ',aktuellerSchueler.Alter);
    END;
  END;
  Close(F);
  {I+}
END;

PROCEDURE anzeigen(nummer: Integer);
 VAR aktuellerSchueler: tschueler;
BEGIN
 {I-}
 Assign(F,_fname);
 Reset(F);
 IF (IOResult<>0) THEN BEGIN
    WriteLn('Datei ',_fname,' konnte nicht ge�ffnet werden!');
    Halt(0);
 END;
 Seek(F,nummer);
 WriteLn('Dateizeiger an Position: ',filepos(F));
 Read(F,aktuellerSchueler);
 WriteLn('Name: ',aktuellerSchueler.Name,'  Vorname: ',aktuellerSchueler.Vorname,
              'Schule: ',aktuellerSchueler.Schule,' Klasse: ',aktuellerSchueler.Klasse,
              'Alter: ',aktuellerSchueler.Alter);
 Close(F);
 {I+}
END;

PROCEDURE aendern(nummer: Integer);
 VAR aktuellerSchueler: tschueler;
BEGIN
  {I-}
  Assign(F,_fname);
  Reset(F);
  IF (IOResult<>0) THEN BEGIN
    WriteLn('Datei ',_fname,' konnte nicht ge�ffnet werden!');
    Halt(0);
  END;
  Seek(F,nummer);
  WriteLn('Sch�lereintrag ',nummer,' �ndern');
  WriteLn('===============================');
  Write(' neuer Name: ');
  ReadLn(aktuellerSchueler.name);
  Write(' neuer Vorname: ');
  ReadLn(aktuellerSchueler.vorname);
  Write(' neue Schule: ');
  ReadLn(aktuellerSchueler.schule);
  Write(' neue Klasse: ');
  ReadLn(aktuellerSchueler.klasse);
  Write(' neues Alter:');
  ReadLn(aktuellerSchueler.alter);
  Write(F,aktuellerSchueler);
  Close(F);
  {I+}
END;

PROCEDURE hinzufuegen;
 VAR aktuellerSchueler: tschueler;
BEGIN
  {I-}
  Assign(F,_fname);
  Reset(F);
  IF (IOResult<>0) THEN BEGIN
    WriteLn('Datei ',_fname,' konnte nicht ge�ffnet werden!');
    Halt(0);
  END;
  WriteLn('Sch�lereintrag anf�gen');
  WriteLn('======================');
  Write(' neuer Name: ');
  ReadLn(aktuellerSchueler.name);
  Write(' neuer Vorname: ');
  ReadLn(aktuellerSchueler.vorname);
  Write(' neue Schule: ');
  ReadLn(aktuellerSchueler.schule);
  Write(' neue Klasse: ');
  ReadLn(aktuellerSchueler.klasse);
  Write(' neues Alter:');
  ReadLn(aktuellerSchueler.alter);
  Seek(F,FileSize(F));
  Write(F,aktuellerSchueler);
  Close(F);
  {I+}
END;

(* Hauptprogramm *)
BEGIN
  ClrScr;
  REPEAT
    ClrScr;
    WriteLn('W�hlen Sie eine Aktion (nur Zahl):');
    WriteLn(' ung�ltige Aktionen wirken wie Aktion ',_BEENDEN);
    WriteLn('(',_ALLESANZEIGEN,') alle Sch�ler anzeigen');
    WriteLn('(',_ANZEIGEN,') bestimmten Sch�ler anzeigen');
    WriteLn('(',_AENDERN,') bestimmten Sch�ler bearbeiten');
    WriteLn('(',_HINZU,') neuen Sch�ler hinzuf�gen');
    WriteLn('(',_BEENDEN,') Programm beenden');
    Write('Ihre Wahl:');
    ReadLn(auswahl);
    CASE (auswahl) OF
      _ALLESANZEIGEN: BEGIN
                        allesanzeigen;
                        WriteLn('Enter zum Best�tigen dr�cken!');
                        ReadLn;
                      END;
      _ANZEIGEN: BEGIN
                   WriteLn('ANZEIGEN:');
                   WriteLn('=========');
                   ReadLn(schuelernummer);
                   anzeigen(schuelernummer);
                   WriteLn('Enter zum Best�tigen dr�cken!');
                   ReadLn;
                 END;
      _AENDERN: BEGIN
                  WriteLn('�nderung:');
                  WriteLn('=========');
                  ReadLn(schuelernummer);
                  aendern(schuelernummer);
                END;
      _HINZU: BEGIN
                hinzufuegen;
              END;
      _BEENDEN: BEGIN
                  WriteLn('Programm wird ordnungsgem�� beendet');
                END;
      ELSE BEGIN
        auswahl:=_BEENDEN;
        WriteLn('Programm wird aufgrund unzul�ssiger Eingabe beendet!');
      END;
    END;
  UNTIL (auswahl=_BEENDEN);
END.
/*
Ren� Haberland				Dresden, den 07.10.2003

dieses Programm erkennt Palindrome; das sind Zeichenketten,
die von links nach rechts und anders herum identisch sind.
Beispiel: 'anna', 'lagerregal', ...

Grundlage des Programms bildet ein Stapel, der durch ein
nach unten wachsendes Feld realisiert wird.

  stapel:

Beispiel: 'abccb' soll gepr�ft werden.
    Stapel: '\0'  (leer)
 1.) 'a' gelesen und auf Stapel gelegt
    Stapel: a
 2.) 'b'  ~
    Stapel: ba
 3.) 'c'  ~
    Stapel: cba
 4.) 'c' gelesen. Weil auf Stapel oben bereits 'c',
        wird das gelesene 'c' verworfen und das
		oberste Element entfernt.
	Stapel: ba
 5.) 'b' gelesen. auf Stapel ist 'b' 
          -> oberstes Element entfernen
	Stapel: a
 6.) '\0' gelesen (Ende der Zeichenkette erreicht)
     => Ende.
	 Da der Stapel nicht leer ist -> kein Palindrome!
----------------------------
|   |   |           |   |  |
|   |   | ......... |   | a|
----------------------------
					  ^
				   <- szeiger
*/
#include <conio.h>
#include <iostream.h>
#include <stdio.h>
#include <string.h>

#define TRUE  1
#define FALSE 0
#define EOS '\0' // EOS = ENDOFSTRING
#define MAXTIEFE 5

char stapel[MAXTIEFE];
int szeiger; // Zeiger auf das n�chste leere Element in 'stapel'

// lege das c auf den Stapel drauf
void push(char c)
{
	if ((szeiger!=0)||(c!=EOS))
	{
		stapel[szeiger]=c;
		szeiger--;
		return;
	}
	printf("Fehler: Stapel ist voll!\n");
}

// nimm das oberste Element vom Stapel
char pop()
{
	char c;
	if (szeiger!=MAXTIEFE)
	{
		szeiger++;
		c=stapel[szeiger];
		stapel[szeiger]=EOS;
		return c;
	}
	return EOS;
}

// pr�fe, ob es sich um ein Palindrom handelt
int palindrom(char* wort)
{
	int i;
	char h;  // Hilfsvariable
	for (i=0;i<(int)strlen(wort);i++)
	{
		h=pop();
		if (h!=wort[i])
		{
			push(h);
			push(wort[i]);
		}
	}
	return ((szeiger==(MAXTIEFE-1))?TRUE:FALSE);
}

void main()
{
	char string[2*MAXTIEFE];  // Stapel brauch nur halbsogross zu sein

	szeiger=MAXTIEFE;
	printf("Geben Sie eine Zeichenkette ein:");
	scanf("%s",string);
	if (palindrom(string)) printf("Zeichenkette ist ein Palindrom.");
	else printf("Zeichenkette ist kein Palindrom.");
	printf("\n Druecke Taste zum Beenden!");
	getch();
}
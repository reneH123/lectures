#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>
#include <conio.h>
#include <time.h>

#define NULL 0

int summe(int n)
{
	int sum=0;
	for (int i=1;i<n+1;i++)
		sum+=i;
	return sum;
}

int ggT(int a, int b)
{
	int x=a,y=b,h;
	while (y!=0)
	{
		h=x;
		x=y;
		y=h%y;
		cout << "x:" << x << "y:" << y << "\n";
	}
	return x;
}

void wuerfeln()
{
	int zzahl,versuche=0;
	srand((unsigned int)time(NULL));
	do{
		zzahl=(rand()%6)+1;
		cout << "gewuerfelte Zahl: " << zzahl << "\n";
		versuche++;
	}while(zzahl!=6);
	cout << "Anzahl benoetigter Durchlaeufe: " << versuche << "\n";
}

void main()
{
	int a,b,n;

	cout << "1.) ggT-Berechnung " << "\n";
	cout << "geben Sie die erste Zahl ein:";
	cin >> a;
	cout << "\n" << "geben Sie die zweite Zahl ein:";
	cin >> b;
	cout << "der ggT(" << a << ";" << b << ")=" << ggT(a,b) << "\n";
	
	cout << "2.) Summe der ersten n Zahlen" << "\n";
	cout << "n=";
	cin >> n;
	cout << "das Ergebnis = " << summe(n) << "\n";

	cout << "3.) Zufallswuerfeln: " << "\n";
	wuerfeln();

	cout << "Zum Beenden Enter druecken!" << "\n";
	getch();
}
program schleifen;
uses crt;

const null: integer=0;
var a,b,n: integer;

function summe(n: integer): integer;
 var i,sum: integer;
begin
  sum:=0;
  for i:=1 to n do begin
    inc(sum,i);
  end;
  summe:=sum;
end;

function ggt(a,b: integer): integer;
 var x,y,h: integer;
begin
  x:=a;
  y:=b;
  while (y<>0) do begin
     h:=x;
     x:=y;
     y:=h mod y;
     writeln('X: ',x,' Y:',y);
  end;
  ggt:=x;
end;

procedure wuerfeln;
 var zzahl,versuche: integer;
begin
  versuche:=0;
  randomize;
  repeat
    zzahl:=random(6)+1;
    writeln('gewuerflte Zahl',zzahl);
    inc(versuche);
  until zzahl=6;
  writeln('Anzahl benoetigter Durchlaeufe: ',versuche);
end;

begin
  writeln('1.) ggT-Berechnung');
  write('geben Sie die erste Zahl ein:');
  readln(a);
  write('geben Sie die zweite Zahl ein:');
  readln(b);
  writeln('der ggT(',a,';',b,')=',ggT(a,b));

  writeln('2.) Summe der ersten n Zahlen');
  write('n=');
  readln(n);
  writeln('das Ergebnis = ',summe(n));

  writeln('3.) Zufallswuerfeln:');
  wuerfeln;

  writeln('Zum Beenden Enter druecken!');
  readln;
end.
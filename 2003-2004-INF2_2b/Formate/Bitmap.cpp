#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <graphics.h>
#include <memory.h>
#include <malloc.h>

struct image{
  unsigned long sizeX;
  unsigned long sizeY;
  char* data;
};

class tools{
private:
  static unsigned int getint(FILE* fp)
  {
	  int c, c1, c2, c3;
	  // get 4 bytes
	  c = getc(fp);
	  c1 = getc(fp);
	  c2 = getc(fp);
	  c3 = getc(fp);
	  return ((unsigned int) c) +
		     (((unsigned int) c1) << 8) +
		     (((unsigned int) c2) << 16) +
		     (((unsigned int) c3) << 24);
  }
  static unsigned int getshort(FILE* fp)
  {
	  int c, c1;
	  c = getc(fp);
	  c1 = getc(fp);
	  return ((unsigned int) c) + (((unsigned int) c1) << 8);
  }
  public:
  int ImageLoad(char *filename, image *Image);
  void ImageDestroy(image *Image);
};

int tools::ImageLoad(char *filename, image *Image)
{
    FILE *file;
    unsigned long size;
    unsigned long i;
    unsigned short int planes;
    unsigned short int bpp;
    char temp;

    if ((file = fopen(filename, "rb"))==NULL) {
      printf("\nFile Not Found : %s\n",filename);
	  scanf("%c");
      return 0;
    }
    fseek(file, 18, SEEK_CUR);
    Image->sizeX = getint (file);
    Image->sizeY = getint (file);
    size = Image->sizeX * Image->sizeY * 3;
    planes = getshort(file);
    if (planes != 1) {
    printf("Planes from %s is not 1: %u\n", filename, planes);
    return 0;
    }
    bpp = getshort(file);
    if (bpp != 24) {
      printf("Bpp from %s is not 24: %u\n", filename, bpp);
      return 0;
    }
    fseek(file, 24, SEEK_CUR);
    Image->data = (char *) malloc(size);
    if (Image->data == NULL) {
    printf("Error allocating memory for color-corrected image data");
    return 0;
    }
    if ((i = fread(Image->data, size, 1, file)) != 1){
      printf("Error reading image data from %s.\n", filename);
      return 0;
    }
    for (i=0;i<size;i+=3) {
      temp = Image->data[i];
      Image->data[i] = Image->data[i+2];
      Image->data[i+2] = temp;
    }
    printf("\n%s successfully loaded\n",filename);
    return 1;
}

/*
  Gib reservierten Arbeitsspeicher der Bitmap wieder frei
*/
void tools::ImageDestroy(image *Image)
{
  free(Image);
  Image->data=NULL;
  Image->sizeX=Image->sizeY=0;
}

// zeichne bitmap
void printbitmap(image *Image)
{
  unsigned long i,j;
  unsigned char color;
  unsigned char r,g,b;

  for (j=0;j<Image->sizeY;j++)
  {
    for (i=0;i<Image->sizeX;i+=3)
    {
      r=Image->data[j*Image->sizeX+i];
      g=Image->data[j*Image->sizeX+i+1];
      b=Image->data[j*Image->sizeX+i+2];
      color=r;
      color<<=5;
      color&=g;
      color<<=2;
      color&=b;
      putpixel(i,j,color);
    }
  }
}

void init(image* I)
{
  char* fn = "test.bmp";
  int gdriver = DETECT, gmode, errorcode;
  tools T;

  if (!(T.ImageLoad(fn,I)))
  {
    printf("Fehler beim Lesen der Bitmap-Datei %s",fn);
    exit(-1);
  }
  initgraph(&gdriver, &gmode, "");
  errorcode = graphresult();
  if (errorcode!=grOk)
  {
     printf("Graphics error: %s\n", grapherrormsg(errorcode));
     exit(-1);
  }
}

void done(image* I)
{
  tools T;

  getch();
  T.ImageDestroy(I);
  closegraph();
}

void main()
{
  image* I;

  init(I);
  printbitmap(I);
  done(I);
}
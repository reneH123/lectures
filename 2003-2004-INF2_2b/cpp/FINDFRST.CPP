#include <stdio.h>
#include <iostream.h>
#include <io.h>
#include <conio.h>
#include <dir.h>

void zeitsetzen(char name[])
{
   FILE *datei;
   ftime dateizeit;
   dateizeit.ft_tsec = 1;              // erst mal eine Zeit vorgeben
   dateizeit.ft_min = 1;            // n�mlich den 1.1.2001, 1:01 Uhr
   dateizeit.ft_hour = 1;
   dateizeit.ft_day = 1;
   dateizeit.ft_month = 1;
   dateizeit.ft_year = 21;
   datei=fopen(name,"r");                 // Datei muss ge�ffnet sein
   setftime(fileno(datei),&dateizeit);// neuen "Zeitstempel" zuweisen
   fclose(datei);
}

void dirinhalt()
{
   ffblk eintrag;
   char datname[]="*.*";                // es sollen alle Dateien und
   int attr=0;                      // Verzeichnisse angezeigt werden
   int fertig;
   fertig = findfirst(datname,&eintrag,attr); // erste passende Datei
   while (!fertig)                                          // suchen
   {
      cout << eintrag.ff_name << "\t";
      cout << eintrag.ff_fsize << "\t";
      cout << int(eintrag.ff_attrib) << "\n";
      fertig = findnext(&eintrag);                    // weitersuchen
   }
}

int main()
{
   clrscr();
   zeitsetzen("zeit.txt");
   dirinhalt();
   return 0;
}

/*
 * 14.12.2003 Ren� Haberland
 *
 * Programm zur Sch�lerverwaltung
 *
 *  Operationen werden sofort auf dauerhaften Speicher durchgef�hrt
 *
 *  nach jeder I/O Operation mu� die Ende-Marke neu gepr�ft werden
 *  neue Datei anlegen: Modus "ab" (nur anh�ngend bin�r schreibend, neue Datei wird erzeugt)
 *  neuen Datensatz hinzuf�gen: "ab"
 *  vorhandenen Datensatz ver�ndern: "r+b" (bin�r lesen und schreiben)
 *
 */
#include <iostream.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#define ALLESANZEIGEN '0'
#define ANZEIGEN '1'
#define AENDERN '2'
#define HINZU '3'
#define BEENDEN '4'
#define fname "test.dat"

typedef char String50[50];
typedef char String40[40];
typedef unsigned char BYTE;
typedef struct{
  String50 name;
  String40 vorname;
  String50 schule;
  BYTE klasse;
  BYTE alter;
}tschueler;

void  einlesen(tschueler &schueler)
{
  cout<<"neuer Sch�ler:\n"<<endl;
  cout<<"neuer Name:\t";
  cin>>schueler.name;
  cout<<"neuer Vorname:\t";
  cin>>schueler.vorname;
  cout<<"neue Schule:\t";
  cin>>schueler.schule;
  cout<<"neue Klasse:\t";
  cin>>schueler.klasse;
  cout<<"neues Alter:\t";
  cin>>schueler.alter;
}

void ausgeben(tschueler schueler)
{
  cout<<"Name: "<<schueler.name<<"\t"
      <<"Vorname: "<<schueler.vorname<<"\t"
      <<"Schule: "<<schueler.schule<<"\t"
      <<"Klasse: "<<schueler.klasse<<"\t"
      <<"Alter: "<<schueler.alter<<endl;
}

void allesanzeigen()
{
  FILE *f;
  tschueler aktuellerSchueler;

  if ((f=fopen(fname,"rb"))==NULL)
  {
    cout<<"Fehler beim �ffnen der Datei "<<fname<<endl;
    f=fopen(fname,"ab");
    cout<<"Datei neu erstellt!"<<endl;
  }
  else
  {
    while (!feof(f))
    {
      fread(&aktuellerSchueler,sizeof(tschueler),1,f);
      if (!feof(f)) ausgeben(aktuellerSchueler);
    }
  }
  fclose(f);
}

void anzeigen(int nummer)
{
  FILE *f;
  tschueler aktuellerSchueler;
  fpos_t pos;

  if ((f=fopen(fname,"rb"))==NULL)
  {
    cout<<"Datei "<<fname<<"konnte nicht ge�ffnet werden!"<<endl;
    exit(-1);
  }
  fseek(f,nummer*sizeof(tschueler),SEEK_SET);
  if (fgetpos(f,&pos)!=0) cout<<"Fehler beim Ermitteln der aktuellen Dateizeigerposition!"<<endl;
  cout<<"Dateizeiger an Position: "<<(long)pos<<endl;
  fread(&aktuellerSchueler,sizeof(tschueler),1,f);
  ausgeben(aktuellerSchueler);
  fclose(f);
}

void hinzufuegen()
{
  FILE *f;
  tschueler aktuellerSchueler;

  if ((f=fopen(fname, "ab"))==NULL)
  {
    cout<<"Datei "<<fname<<"konnte nicht ge�ffnet werden!"<<endl;
    exit(-1);
  }
  einlesen(aktuellerSchueler);
  fwrite(&aktuellerSchueler,sizeof(tschueler),1,f);
  fclose(f);
}

void aendern(int nummer)
{
  FILE *f;
  tschueler aktuellerSchueler;

  if ((f=fopen(fname,"r+b"))==NULL)
  {
    cout<<"Datei "<<fname<<"konnte nicht ge�ffnet werden!"<<endl;
    exit(-1);
  }
  cout<<"Sch�lereintrag "<<nummer<<"�ndern"<<endl;
  cout<<"==============================="<<endl;
  einlesen(aktuellerSchueler);
  fseek(f,nummer*sizeof(tschueler),SEEK_SET);
  fwrite(&aktuellerSchueler,sizeof(tschueler),1,f);
  fclose(f);
}

void main()
{
  BYTE auswahl,schuelernummer;
  do{
    clrscr();
    cout<<"W�hlen Sie eine Aktion (nur Zahl):"<<endl;
    cout<<"\t\tung�ltige Aktionen wirken wie Aktion"<<BEENDEN<<endl;
    cout<<"("<<ALLESANZEIGEN << ") alle Sch�ler anzeigen"<<endl;
    cout<<"("<<ANZEIGEN<<") bestimmten Sch�ler anzeigen"<<endl;
    cout<<"("<<AENDERN<<") bestimmten Sch�ler bearbeiten"<<endl;
    cout<<"("<<HINZU<<") neuen Sch�ler aufnehmen"<<endl;
    cout<<"("<<BEENDEN<<") Programm beenden"<<endl;
    cout<<"Ihre Wahl:";
    cin>>auswahl;
    switch (auswahl)
    {
      case ALLESANZEIGEN: allesanzeigen(); break;
      case ANZEIGEN: {
		       cout<<"ANZEIGEN:"<<endl;
		       scanf("%d",&schuelernummer);
			   anzeigen(schuelernummer);
		     }break;
      case HINZU: {
		    cout<<"HINZUF�GEN:"<<endl;
		    hinzufuegen();
		  }break;
      case AENDERN: {
		       cout<<"�NDERUNG:"<<endl;
		       aendern(schuelernummer);
		    }break;
      case BEENDEN: cout<<"Programm wird ordnungsgem�� beendet."; break;
      default: {
		 auswahl=BEENDEN;
		 cout<<"Programm wird aufgrund unzul�ssiger Eingabe beendet!\n\n"<<endl;
	      }break;
    }
    cout<<"Bitte Taste dr�cken!"<<endl;
    getch();
  }while (auswahl!=BEENDEN);
}
#include <iostream.h>
#include <stdio.h>

typedef struct
	{ char titel[20];
	  char autor[20];
	  float preis;
	  int ausgeliehen;
	} tbuch;

char st[12];    // Dateiname, muss hier global sein
FILE* buchdatei;

void ausgeben(tbuch b)
	{
	  cout << "Titel      " << b.titel << endl;
	  cout << "Autor      " << b.autor << endl;
	  cout << "Preis      DM" << b.preis << endl;
	  if (! b.ausgeliehen) cout << "nicht ";
	  cout << "ausgeliehen" << endl;
	}

void eingeben(tbuch &b)
	{
	  char h;
	  cout << "Titel        ";
	  cin >> b.titel;
	  cout << "Autor        ";
	  cin >> b.autor;
	  cout << "Preis        ";
	  cin >> b.preis;
	  cout << "ausgeliehen? ";
	  cin >> h;
	  b.ausgeliehen=(h=='J' || h=='j');
	}

void oeffnen()                  // hier auch als anlegen
	{
	  cout << "Name der Datei: ";
	  cin >> st;
	}

void nummer_waehlen(int &nr)
	{
	  cout << "Nummer des Datensatzes: ";
	  cin >> nr;        // erst mal ohne Test, wegen filesize
	}

void laden(tbuch &buch, int nr)
	{ buchdatei=fopen(st, "rb");   // bin�r, nur zum Lesen
	  fseek(buchdatei, (nr-1)*sizeof(tbuch), SEEK_SET);
	  fread(&buch, sizeof(tbuch), 1, buchdatei);
	  fclose(buchdatei);
	}

void speichern(tbuch &buch)
	{ buchdatei=fopen(st, "ab");   // bin�r, nur zum Schreiben (anh�ngen)
	  fwrite(&buch, sizeof(tbuch), 1, buchdatei);
	  fclose(buchdatei);
	}

void alle_ausgeben()
	{ tbuch buch;
	  buchdatei=fopen(st, "rb");   // bin�r, nur zum Lesen
	  while (!feof(buchdatei))
	    { fread(&buch, sizeof(tbuch), 1, buchdatei);
	      if (!feof(buchdatei)) ausgeben(buch);
            }
	  fclose(buchdatei);
	}

void menue()
	{
	  char c;
	  int n;
	  tbuch b;
	  do
	    {
	      cout << "Bibliotheksverwaltung" << endl;
	      cout << "Datei ausw�hlen          d" << endl;
	      cout << "Datensatz lesen          l" << endl;
	      cout << "Alle anzeigen            a" << endl;
	      cout << "Datensatz schreiben      s" << endl;
	      cout << "Ende                     e" << endl;
	      cin >> c;
	      switch (c)
		{ case 'd' : oeffnen(); break;
		  case 'l' : {
			       nummer_waehlen(n);
			       laden(b,n);
			       ausgeben(b);
			       break;
			     }
		  case 'a' : {
			       alle_ausgeben();
			       break;
			     }
		  case 's' : {
			       eingeben(b);
			       speichern(b);
			       break;
			     }
		}
	     }
	  while (c!='e');
	}

int main()
	{
	  menue();
	  return 0;
	}

(* 
  14.12.2003 Ren� Haberland
  dieses Programm ist zum Einlesen von Anfangs-Daten gedacht 
*)
PROGRAM Schueler0;
USES Crt;

TYPE  tschueler=RECORD
        name: String[50];
        vorname: String[40];
        schule: String[50];
        klasse: Byte;
        alter: Byte;
      END;
VAR F: FILE OF tschueler;
    c: Char;
    abbruch: Boolean;
    aktuellerSchueler: tschueler;

PROCEDURE LeseSchueler(VAR schueler: tschueler);
BEGIN
  WriteLn('neuer Sch�lereintrag');
  WriteLn('====================');
  Write(' Name: ');
  ReadLn(schueler.name);
  Write(' Vorname: ');
  ReadLn(schueler.vorname);
  Write(' Schule: ');
  ReadLn(schueler.schule);
  Write(' Klasse: ');
  ReadLn(schueler.klasse);
  Write(' Alter:');
  ReadLn(schueler.alter);
END;

BEGIN
  {I-}
  WriteLn('Programm zur Aufnahme von neuen Sch�ler-Daten');
  WriteLn;
  Assign(F,'test.dat');
  Rewrite(F);
  abbruch:=false;
  WHILE Not(abbruch) DO BEGIN
    WriteLn('neuen Sch�ler aufnehmen? (j)a (n)ein ');
    ReadLn(c);
    IF ((c='j')OR(c='J')) THEN BEGIN
     LeseSchueler(aktuellerSchueler);
     Write(F,aktuellerSchueler);
    END
    ELSE abbruch:=true;
  END;
  Close(F);
  {I+}
END.
-- simple pattern matching
mysum::Num a=>[a]->a
mysum []=0
mysum [x]=x
mysum [x,y]=x+y
mysum [x,y,z]=x+y+z

sum2::Num a=>[a]->a
-- e.g. sum2::[Int]->Int
sum2 [] = 0
sum2 (h:t)=h+sum t

fib::Int->Int
fib 1=1
fib 2=1
fib n=fib(n-1)+fib(n-2)

_member::Eq a=>a->[a]->Bool
_member x [] = False
_member x (h:_) | x==h =True
_member x (_:d) = _member x d

first::(a,b)->a
first (e,f) = e
second::(a,b)->b
second(e,f)=f
--smaller::(Num,Num)->Num
--smaller(e,f) = if e<f then e else f

mymin2::[Int]->Int->Int
mymin2 (h:t) cmp
 | t==[] = if h<cmp then h else cmp
 | h<cmp = mymin2 t h
 | otherwise = mymin2 t cmp

mymin::[Int]->Int
mymin (h:t) = if t==[] then h else mymin2 t h


offset::Int
offset = ord 'a'-ord 'A'

capitalize::Char->Char
capitalize c = if isLower c then chr (ord c-offset) else c
 
idInt::Int->Int
idInt x=x

idInteger::Integer->Integer
idInteger x=x

sign::Integer->Integer
-- sign::Float->Float
sign x
 | x<0 = -1
 | x==0 = 0
 | x>0 = 1

-- cube::Integer->Integer
cube::Float->Float
cube x = x*x*x

npot::Integer->Integer->Integer
npot base exp 
 | exp==0 = 1
 | otherwise = base*(npot base (exp-1))

mult::Integer->Integer->Integer
mult m n
 | n==0 = 0
 | m==0 = 0
 | m==1 = n
 | n==1 = m
 | otherwise = m+(mult m (n-1))

logand::[Bool]->Bool
logand l
 | l==[] = True
 | head l==True  = logand (tail l)
 | head l==False = False
 | otherwise = False

logxor::Bool->Bool->Bool
logxor a b=((not a && b)||(a && not b))

qsort []=[]
qsort (x:xs)=qsort[u|u<-xs,u<=x]++[x]++qsort[u|u<-xs,u>x]

-- further tasks suggested:
--  1) try to simulate a stack and other abstract data types
--  2) write a parser (for example for XML, HTML, with some simple grammars)
--  3) symbolic derivations

import IO

leapyear::Int->Bool
leapyear = \y -> (mod y 4 == 0) && ((mod y 100 /= 0) || (mod y 400 == 0))

main::IO ()
main = do {
       hSetBuffering stdout NoBuffering >>
       putStr "--leap year calculation-- \n" >>
       putStr "Year: " >>
       readLine >>= \ y ->
       if leapyear(read y) then putStr("is leap year\n")
                           else putStr("is not leap year \n")
       }
  where readLine = isEOF >>= \eof -> 
       if eof then return []
          else getChar >>= c->
            if c 'elem' ['\n','\r'] then return []
               else readLine >>= \cs->return (c:cs)

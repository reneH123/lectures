--data AExp = Ident
-- | Int
-- | Plus AExp AExp
  
type Parser tok a = [tok]->[(a,[tok])]

fail::Parser a b
fail x = []

succeed::a->Parser tok a
succeed value toks = [(value,toks)]

just::Parser a b ->Parser a b
just p = (filter (null . snd)) . p

-- ((1+2)+3)
--tl1 = [((Plus Plus 1 2 3),[])]


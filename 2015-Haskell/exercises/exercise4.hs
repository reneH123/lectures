-- helper functions
ismember::Eq a=>a->[a]->Bool
ismember a [] = False
ismember a (x:xs)
 | a==x = True
 | otherwise = ismember a xs

ismember2::Eq a=>a->[a]->Bool
ismember2 x l = or (map (==x) l)

-- check whether has duplicates
dupl,set::Eq a=>[a]->Bool
dupl []     = False
dupl (x:xs) = (ismember2 x xs) || (dupl xs)

my_length::[a]->Int
my_length []     = 0
my_length (x:xs) = 1+(my_length xs)

-- Task 1
my_take,my_drop::Int->[a]->[a]
my_take n []     = []
my_take 0 l      = []
my_take n (x:xs) = x:(my_take (n-1) xs)

-- Task 2
my_drop n []     = []
my_drop 0 l      = l
my_drop n (x:xs) = my_drop (n-1) xs

-- Task 3
set l = not (dupl l)

-- Task 4
pos::Eq a=>[a]->a->Int->Int->[Int]
pos [] pattern j i = [] -- @(i == j+1)
pos (x:xs) pattern i maxi 
 | x==pattern = (i+1):(pos xs pattern (i+1) maxi)
 | otherwise  = (pos xs pattern (i+1) maxi)

positions::Eq a=>[a]->a->[Int]
positions l x = pos l x 0 ((my_length l)-1)

-- Task 5
--dups in out
dups::Eq a=>[a]->[a]->[a]
dups []     lout = lout
dups (x:xs) lout 
 | (ismember x xs)&&(not (ismember x lout)) 
     = dups xs (x:lout)
 | otherwise = dups xs lout

duplicates::Eq a=>[a]->[a]
duplicates l = dups l []

-- Task 6
-- a)
power,power2,my_power::Int->Int->Int
power k 0 = 1
power k n = k * (power k (n-1))

my_power k n = foldr (*) 1 kth where kth = (map (\x->k) [1..n])

-- b)
-- power 3 3 = 3 * (power 3 (3-1)) = 3*(power 3 2)
-- = 3*(3*(power 3 (2-1)))
-- = 3*(3*(power 3 1))
-- = 3*(3*(3*(power 3 (1-1))))
-- = 3*(3*(3*(power 3 0)))
-- = 3*(3*(3*1)))
-- = 27

-- c)
power2 k n
 | n==0 = 1
 | ((n `mod` 2)==0) = (power2 k (n `div` 2))^2
 | ((n `mod` 2)==1) = k*(power2 k ((n-1) `div` 2))^2

-- Task 7
-- a)
pumpZO::Int->String
pumpZO 0 = ""
pumpZO n = "0" ++ pumpZO (n-1) ++ "1"
--pump 5 = ["finish"]
pump::Int->[String]
pump n = (pumpZO n):pump (n+1)

zeroone::[String]
zeroone = pump 0
-- b)
fib::Int->Int
fib 1 = 1
fib 2 = 1
fib n = fib (n-1)+fib(n-2)

genfibs::Int->[Int]
genfibs 0 = []
genfibs n = fib n : genfibs (n-1)

generateFibs::Int->[Int]
generateFibs n = reverse (genfibs n)

-- Task 8
-- a) with pattern matching and recursion
numChar,numChar2,numChar3::Char->String->Int
numChar c l
 | l==""       = 0
 | c==(head l) = 1+(numChar c (tail l))
 | otherwise   = numChar c (tail l)
-- b) using foldr
--numChar2 c l = foldr (\x->\y->if (x==c) then +1 else +0) 0 l

numChar2 c l = foldr (\x y->if (x==c) then y+1 else y) 0 l

-- c) with list abstraction
numChar3 c l = sum (map (\x->if x==c then 1 else 0) l)

-- Task 9
-- a) map map
--map (+1) [1,2,3] --> [1+1,2+1,3+1] = [2,3,4]

-- map::(a->b)->[a]->[b]
-- =>
-- map map 
-- = map (a->b)->[a]->[b]
-- = ((a->b)->[a]->[b]) ->[a]->[b] 
--    und 
--   a=(a->b)
--   b=[a]->[b]
-- = ((a->b)->[a]->[b]) ->[a->b]->[[a]->[b]] 
-- since 1st type is dropped this implies:
-- = [a->b]->[[a]->[b]]

-- b) map foldr
-- map::(a->b)->[a]->[b]
-- foldr::(a->b->b)->b->[a]->b
-- = map (a->b->b)->b->[a]->b
-- = ((a->b->b)->b->[a]->b) ->[a]->[b]
--   und
--  a=(a->b->b)
--  b=b->[a]->b
-- = [a->b->b]->[b->[a]->b]

-- c) foldr map
-- foldr::(a->b->b)->b->[a]->b
-- map::(a->b)->[a]->[b]
-- = foldr (a->b)->[a]->[b]
-- = ((a->b)->[a]->[b])->b->[a]->b
--    und
--   a=a->b
--   b=[a] oder [b]
-- = (I)  [a]->[a->b]->[a]
--   (II) [b]->[a->b]->[b]
-- = where a=b gilt (I)=(II)
-- so, there there exists a common type: [a]->[a->a]->[a]

-- Task 10
-- a)
f1::a->Bool
f1 x = True
-- b)
f2::a->a->b->(b,a)
f2 x y z = (z,y)
-- c)
-- f3::a->b
-- not possible, because b has to be a finite constant terminal (then b would not be polymorphic anymore!)


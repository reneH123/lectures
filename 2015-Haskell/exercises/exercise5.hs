qsort::[Int]->[Int]
bsort::[Int]->[Int]
msort::[Int]->[Int]

isort2::[Int]->[Int]->[Int]
insert::Int->[Int]->[Int]
merge::[Int]->[Int]->[Int]

insert x l =  (filter (<x) l ++ [x] ++ filter (>=x) l)

-- InsertSort
isort2 [] ys     = ys
isort2 (x:xs) ys = isort2 xs (insert x ys)

isort xs = isort2 xs []

-- QuickSort
qsort [] = []
qsort (x:xs) = qsort(filter (<x) xs) ++ [x] ++ qsort(filter (>=x) xs)

-- BubbleSort
bsort [] = []
bsort l = min : (bsort (filter (>min) l)) where min=minimum l

-- MergeSort
merge [] ys     = ys
--merge (x:xs) l  = merge xs (insert x l)
--merge (x:xs) l  = foldl (insert l) [] l

msort xs
   | length xs <= 1     = xs
   | otherwise  = merge (msort (take 1 xs)) (msort (drop 1 xs))

-- binary tree
data Tree a = Leaf |
	      Node a (Tree a) (Tree a)

heightTree::Tree a->Int
widthTree::Tree a->Int
countNodes::Tree a->Int
fringe::Tree a->[a]
leafs::Tree a->[a]
checkBalanced::Tree a->Bool

heightTree Leaf = 0
heightTree (Node x l r) = 1+ (max (heightTree l) (heightTree r))

widthTree n = 2^(heightTree n)

countNodes Leaf = 0
countNodes (Node x l r)=1+(countNodes l)+(countNodes r)

checkBalanced Leaf = True
checkBalanced (Node x l r) = if (abs (heightTree l - heightTree r) <=1) then True else False

fringe Leaf = []
fringe (Node x l r) = [x] ++ (fringe l) ++ (fringe r)

leafs Leaf = []
leafs (Node x Leaf Leaf) = [x]
leafs (Node x l r) = (leafs l) ++ (leafs r)

deleteNodes::Tree a->[Tree a]
deleteNodes Leaf = [Leaf]
deleteNodes (Node x l r) = (deleteNodes l) ++ (deleteNodes r)

deleteLeafs::Tree a->[a]
deleteLeafs Leaf = []
deleteLeafs (Node x l r) = [x] ++ (deleteLeafs l) ++ (deleteLeafs r)

-- deleteLeafs (Node 23 (Node 19 Leaf Leaf) (Node 11 Leaf (Node 19 Leaf Leaf)))
-- -> [23,19,11,19]

tree_map::(a->b)->Tree a->Tree b
tree_map f Leaf = Leaf
tree_map f (Node x l r) = Node (f x) (tree_map f l) (tree_map f r)

-- fringe (tree_map (\x->x+1) (Node (1+1) Leaf (Node (1+2) Leaf Leaf)))
-- ->[3,4]
-- fringe (tree_map (\x->x+1) (Node (1+1) Leaf (Node (1+2) Leaf Leaf)))
-- -> [True,False]

tree_foldr::(a->a->a)->a->Tree a->a
tree_foldr f e Leaf = e

tree_foldr f e (Node x l r) =  f x (f (tree_foldr f e l) (tree_foldr f e r))

-- tree_foldr (+) 0 (Node 23 Leaf (Node 31 Leaf Leaf))
-- -> 54

data Tree a = Leaf | 
              Node a (Tree a) (Tree a)
              deriving Show
data GTree a = GNode a [GTree a]  
               deriving Show       

height Leaf = 0
height (Node x l r) = 1 + (max (height l)(height r))

tree_map f Leaf = Leaf
tree_map f (Node x l r) = Node (f x) (tree_map f l) (tree_map f r)

foldTree::(a->[b]->b)->GTree a->b
foldTree f (GNode x subtrees) = f x (map (foldTree f) subtrees)

f2 = foldTree (GNode.(1.0/))

t1 = (Node (1+1) Leaf (Node (1+2) Leaf Leaf))
t2 = (GNode 2.0 [(GNode 3.0 [])])

-- f2 t2

treeIdentity::GTree a->GTree a
treeIdentity = foldTree (GNode . (\ x -> x))


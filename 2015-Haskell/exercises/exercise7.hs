import Char

-- 1.
myInteract::(Char->Char)->IO ()
myInteract f = interact (map f)

myInteract2 f = getLine >>= 
		\line->putStr((map f line)++"\n") >>
		myInteract2 f

-- 2.
--brackets
check [] [] = (True,[])
check [] s = (False,s)
check (x:xs) [] = check xs [x]
check (x:xs) (y:ys)
 | x==y = check xs ys
 | otherwise = check xs ([x]++(y:ys))


--readAndReverse :: String -> String
--readAndReverse = lines .
--		(mapUntil (==[]) reverse) .
--		unlines
--mapUntil::(a->Bool)->(a->a)->[a]->[a]
--mapUntil p f (x:xs)
--	| (p x) = [f x]
--	| otherwise = (f x):(mapUntil p f xs)
--mapUntil p f x = x

-- 2.
--set::[a]->Bool
--set [] = True
--set (x:xs) = if (ex xs) then False else (set xs)

-- 3.
--pos a (x:xs) =
-- | a==x = 0
-- | otherwise = 1+(pos a xs)
 
--positions::Eq a=>[a]->a->[Int]
--positions l a = positions2 l a 0 []

--positions2::Eq a=>[a]->a->Int->[Int]->[Int]
--positions2 (x:xs) a p res = if (a==x) then positions2 xs a (p+1) (res++[p]) else positions2 xs a (p+1) res

-- 7.
--segments [] = []





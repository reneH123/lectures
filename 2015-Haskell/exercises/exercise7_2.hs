data BTree a = Empty
               | Node a (BTree a) (BTree a)

foldTree f e Empty = e
foldTree f e (Node x l r) = f x (foldTree f e l) (foldTree f e r)
size = foldTree (\n l r -> 1+ l + r) 0

perms (xs) = [(a:b) | a<-xs, b<-perms (filter (/=a) xs)]

-- [0,1,2] => perms =>
-- [[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]


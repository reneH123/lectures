-- Task 01 -------------------------------------------------

take2 :: Int -> [a] -> [a]
take2 0 _ = []
take2 _ [] = []
take2 n (h:t) = h:(take (n-1) t)


-- Task 02 -------------------------------------------------

drop2 :: Int -> [a] -> [a]
drop2 0 list = list
drop2 _ [] = []
drop2 n (h:t) = drop (n-1) t


-- Task 03 -------------------------------------------------

set :: Eq a => [a] -> Bool
set [] = True
set (h:t)
	| (member h t) = False
	| otherwise = (set t)


member :: Eq a => a -> [a] -> Bool
member _ [] = False
member x (h:t)
	| x == h = True
	| otherwise = member x t


-- Task 04 -------------------------------------------------

positions :: Eq a => [a] -> a -> [Int]
positions = numPos 1


numPos :: Eq a => Int -> [a] -> a -> [Int]
numPos _ [] _ = []
numPos n (h:t) x
	| x == h = n:(numPos (n+1) t x)
	| otherwise = numPos (n+1) t x


-- Task 05 -------------------------------------------------

duplicates :: Eq a => [a] -> [a]
duplicates [] = []
duplicates (h:t)
	| empty (positions t h) = duplicates t
	| otherwise = h:(duplicates t)


empty :: [a] -> Bool
empty list = (length list) == 0


-- Task 06 a) ----------------------------------------------

power :: Int -> Int -> Int
power = (^)


-- Task 06 c) ----------------------------------------------

powerAlt :: Int -> Int -> Int
powerAlt _ 0 = 1
powerAlt k n
	| (mod n 2) == 0 = square (powerAlt k (div n 2))
	| otherwise = k * (square (powerAlt k (div n 2)))


square :: Int -> Int
square a = a^2


-- Task 07 a) ----------------------------------------------

zeroone :: [String]
zeroone = zerooneNList 0


zerooneNList :: Int -> [String]
zerooneNList n = [zerooneN n] ++ (zerooneNList (n+1))


zerooneN :: Int -> String
zerooneN 0 = ""
zerooneN n = "0" ++ (zerooneN (n-1)) ++ "1"


-- Task 07 b) ----------------------------------------------

fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = (fib (n-1)) + (fib (n-2))


fibRep :: Int -> Int
fibRep 0 = 1
fibRep 1 = 1
fibRep n = fibRepMN n 1 1


fibRepMN :: Int -> Int -> Int -> Int
fibRepMN 1 _ n = n
fibRepMN i m n = fibRepMN (i-1) n (m+n)


fibList :: Int -> [Int]
fibList 0 = [1]
fibList 1 = [1,1]
fibList n = fibListMN n 1 1 [1,1]


fibListMN :: Int -> Int -> Int -> [Int] -> [Int]
fibListMN 1 _ _ list = list
fibListMN i m n list = fibListMN (i-1) n (m+n) (list ++ [m+n])


-- Task 08 a) ----------------------------------------------

numCharA :: Char -> String -> Int
numCharA _ [] = 0
numCharA c (h:t)
	| c == h = 1 + (numCharA c t)
	| otherwise = numCharA c t


-- Task 08 b) ----------------------------------------------

numCharB :: Char -> String -> Int
numCharB a list = foldr (updateOccs a) 0 list


updateOccs :: Char -> Char -> Int -> Int
updateOccs a b n
	| a == b = n + 1
	| otherwise = n


-- Task 08 c) ----------------------------------------------

numCharC :: Char -> String -> Int
numCharC a list = length ([x | x <- list, x == a])


-- Extra -----------------------------------------------------

data Nat = Zero | Succ Nat

instance Eq Nat where  
    Zero == Zero = True
    Succ(x) == Succ(y) = x == y
    _ == _ = False
    

instance Ord Nat where
    Zero <= Zero = True
    Zero <= Succ x = True
    Succ x <= Succ y = x <= y
    _ <= _ = False
    x < y = x <= y && x /= y
    

fac 0 = 1
fac n = fact 1 n


fact m 0 = m
fact m n = fact (m * n) (n - 1)


fac2 n = foldr (*) 1 [1..n]


powset :: [a] -> [[a]]
powset [] = [[]]
powset (a:x) = p ++ [a:y | y <- p] where p = powset x


--powset2 :: [a] -> [[a]]
--powset2 [] = [[]]
--powset2 (a:x) = (powset2 x) ++ [a] ++ 


-- some test samples  ----------------------------------------------

tl1 = ["To","be","or","not","to","be"]
tl2 = []
tl3 = [1,4,7,3,2,5,4,12]

ts1 = "hi, this is a string!"


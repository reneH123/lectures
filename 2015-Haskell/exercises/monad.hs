f::a->Int->Maybe a
f x y
 | (y==1) = Just x
 | otherwise = Nothing

myIntersect x y = do x'<-x
                     y'<-y
		     if x'==y' then return x' else []

t = [(x,y)|x<-[1,2,3],y<-[1,2,3],x/=y]

mvLift2:: (a -> b -> c) -> [a] -> [b] -> [c]
mvLift2 f x y =  do x' <- x
                    y' <- y
                    return (f x' y')

--class Monad m where
--	(>>=)	:: m a -> (a -> m b) -> m b
--	(>>)	:: m a -> m b -> m b
--	return	:: a -> m a
--	fail	:: String -> m a
--	m >> k	= m >>= \_ -> k
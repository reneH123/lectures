-- -----------------------------------------------
-- sorting algorithms
-- -----------------------------------------------

-- -----------------------------------------------
-- QUICK-SORT
-- -----------------------------------------------
qsort:: Ord a => [a] -> [a]
qsort []     = []
qsort (x:xs) = (qsort lesser) ++ [x] ++ (qsort greater)
               where lesser  = [y|y <- xs, y<x]
                     greater = [y|y <- xs, y>x]

-- -----------------------------------------------
-- MERGE-SORT
-- -----------------------------------------------
merge [] ys     = ys
merge (x:xs) [] = x:xs
merge (x:xs) (y:ys) 
   | x<=y       =  x:merge xs (y:ys) 
   | x>y        =  y:merge (x:xs) ys

msort:: Ord a   => [a] -> [a]
msort xs
   | n < 2      = xs
   | otherwise  = merge (msort left) (msort right)
                  where left  = take h xs
                        right  = drop h xs
                        n      = length xs
                        h      = div n 2

-- -----------------------------------------------
-- BUBBLE-SORT
-- -----------------------------------------------
bubble:: Ord a  => [a] -> [a]
bubble []       = []
bubble [x]      = [x]
bubble (x:y:xs)
  | x>y         = y : bubble (x:xs)
  | otherwise   = x : bubble (y:xs)

bsort:: Ord a   => [a] -> [a]
bsort  []       = []
bsort (x:xs)    = bsort (init ys) ++ [last ys] 
     where ys   = bubble (x:xs)


-- -----------------------------------------------
-- test samples (unsorted)
-- -----------------------------------------------
m1 = [2,5,3,8,6,7,1,0,9]
m2 = [12,55,23,10,62,53,74,88,29,31,90,11,19,91,15]


t1 = [(x,y)|x<-[1..3],y<-[1..3],x/=y]

sumSquares 0 = 0
sumSquares n = sumSquares (n-1) + (n*n)

--m2 x y = do x <- [1,2,3]
--   y <- [1,2,3]
--   True <- return (x /= y)
--   return (x,y)

--mvLift2                :: (a -> b -> c) -> [a] -> [b] -> [c]
--mvLift2 f x y          =  do x' <- x
                             --y' <- y
                             --return (f x' y')

--m1 = [1,2,3] >>= 
--     (\ x -> [1,2,3] 
--     >>= 
--     (\y -> return (x/=y) 
--     >>=
--   	 (\r -> case r of True -> return (x,y)
--                      _    -> fail "")))


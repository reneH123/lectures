--  s .. state (e.g. ImpState)
--  a .. e.g. Int
newtype StateTrans s a = ST( s -> (s, a) )

-- concrete "return" and ">>=" operations for "StateTrans"
instance Monad (StateTrans s) where
    -- (>>=) :: StateTrans s a -> (a -> StateTrans s b) -> StateTrans s b
    (ST p) >>= k  =  ST( \s0 -> let (s1, a) = p s0
                                    (ST q) = k a
                                in q s1 )
     			   	
    -- return :: a -> StateTrans s a
    return a = ST( \s -> (s, a) )              

-- check: if really needed?
applyST :: StateTrans s a -> s -> (s, a)
applyST (ST p) s = p s

{-- gcd algorithm:
     while x != y do
          if x < y
          then y := y-x
          else x := x-y
     return x 
--}

-- ? type for representing the state before and after
type ImpState = (Int, Int)

getX, getY :: StateTrans ImpState Int
getX = ST(\(x,y)-> ((x,y), x))
getY = ST(\(x,y)-> ((x,y), y))

putX, putY :: Int -> StateTrans ImpState ()
putX x' = ST(\(x,y)->((x',y),()))
putY y' = ST(\(x,y)->((x,y'),()))

gcdST :: StateTrans ImpState Int
gcdST = do x <- getX
           y <- getY
           (if x == y
            then
                 return x
            else if x < y
            then 
                 do putY (y-x)
                    gcdST
            else
                 do putX (x-y)
                    gcdST)
                    
 
greatestCommonDivisor x y = snd( applyST gcdST (x,y) )                    

{-- call: greatestCommonDivisor 135 42 
    returns: 3 --}


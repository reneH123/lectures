newtype StateTrans s a = ST(s->(s,a))

instance Monad (StateTrans s) where
    (ST p) >>= k  =  ST(\s0->let (s1,a) = p s0
                                 (ST q) = k a
                             in q s1 )
    return a = ST(\s->(s,a))

getX = ST(\(x,y)->((x,y),x))
getY = ST(\(x,y)->((x,y),y))
setX x' = ST(\(x,y)->((x',y),()))
setY y' = ST(\(x,y)->((x,y'),()))

gcdST = do
   x<-getX
   y<-getY
   (if x==y then return x
    else 
    	if x<y then 
    	  do setY (y-x) 
    	     gcdST
      else do setX (x-y) 
              gcdST)

detGCD a b = snd (let (ST p) = gcdST in p (a,b))

{--
h:=0;
for i:=1 to n do begin
  h:=h+(i*i);
end;
sumSquares(n) = h
--}


{--
while (y<>0) do begin
  y:=x;
  x:=x mod y;
end;
gcd(x,y) = x
--}


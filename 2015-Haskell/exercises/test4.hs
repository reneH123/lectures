-- lists as monad
instance Monad [ ] where
    xs >>= f   =  concat (map f xs)
    return x  = [x]

-- some user interaction

askFor::String->IO String
askFor s = do putStr s
              getLine

main::IO ()
main = do file <- askFor "fileName: "
	  cnts <- readFile file
	  putStr cnts


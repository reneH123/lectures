-- complex numbers
data Cmplx = Complex {real::Int,imaginaer::Int}

instance Show Cmplx where
  -- "show" has implicite type ::Show Cmplx=>Cmplx->String
  show (Complex {real=r,imaginaer=i})= (show r) ++ "+" ++ (show i) ++ "i"

-- GEOMETRY: triangle

--class Cmplx r i where
 --show::Show Cmplx=>Cmplx->String
--class Geom a where
--  t1::Int->Int

-- matrices
data Vector a= Vec [a]
 deriving (Eq, Show)

data Matrix a = Mat [Vector a]
 deriving (Eq, Show)

nth::Int->Vector a->a
nth 0 (Vec (x:xs))  = x
nth n (Vec l) = nth (n-1) (Vec (tail l))

nthM::Int->Matrix a->Vector a
nthM 0 (Mat (x:xs)) = x
nthM n (Mat l) = nthM (n-1) (Mat (tail l))

identity= Mat [Vec [1,0,0], Vec [0,1,0], Vec [0,0,1]]

-- days of week

data Day = Mon | Tue | Wed | Thu | Fr | Sa | Su
 deriving (Eq, Enum)

nextDay::Day->Day
nextDay d = toEnum (((fromEnum d) +1) `mod` 7)

prevDay::Day->Day
prevDay d = toEnum (((fromEnum d) -1) `mod` 7)

daysTil::Day->[Day]
daysTil d = [toEnum(t2) | t2<-[0..fromEnum(d)]]

{- strictly speaking: is not explicitly required
   Default equality matches defined ones.
instance Eq Day where
 Mon == Mon = True
 Tue == Tue = True
 Wed == Wed = True
 Thu == Thu = True
 Fr == Fr = True
 Sa == Sa = True
 Su == Su= True
 x == y = False
 x/=y = not (x==y)-}

instance Ord Day where
 Mon < Tue = True
 Mon < Wed = True
 Mon < Thu = True
 Mon < Fr = True
 Mon < Sa = True
 Mon < Su = True
 Tue < Wed = True
 Tue < Thu = True
 Tue < Fr = True
 Tue < Sa = True
 Tue < Su = True
 Wed < Thu = True
 Wed < Fr = True
 Wed < Sa = True
 Wed < Su = True
 Thu < Fr = True
 Thu < Sa = True
 Thu < Su = True
 Fr < Sa = True
 Fr < Su = True
 Sa < Su = True
 x < y = False
 x <= y = x < y || x==y
 x > y = not(x<=y)
 x >= y = x>y || x==y

instance Show Day where
 show Mon = "Monday"
 show Tue = "Tuesday"
 show Wed = "Wednesday"
 show Thu = "Thursday"
 show Fr = "Friday"
 show Sa = "Saturday"
 show Su = "Sunday"



-------------------------

-- define new type composed by existing ones
type ListType a = [a]

-- \> t3 [3,2,1]
-- 3
t3::ListType a->a
t3 (x:xs) = x

data TPerson = Person {name,surname :: [Char], age :: Int}
type TPersons = [TPerson]


-- data type definition
data C = D {b::Bool} | E {i::Int} | Int

-- ?
t1::C->Bool
t1 E {i=ii}= False
--t1 D = True


-- \> t2 20
-- False
t2::Int->Bool
t2 _ = False


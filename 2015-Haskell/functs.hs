my_member a l = if length (filter (==True) (map (==a) l))>0 then True else False
my_sum2 l = foldr (+) 0 l
--remark: fib not simply possible using foldr, because not an array!

my_min (x:xs)  = foldr (min) x xs
my_mult l = foldr (*) 1 l

successor::Num a=>a->a
successor x = x+1

square::Num a=>a->a
square x = x*x

twice::Num a=>(a->a)->a->a
twice f x = f (f x)

quad::Num a=>a->a
quad = twice square

--_curry::(a->b->c)->a->b->c
_curry::((a,b)->c)->a->b->c
_curry f x y = f (x,y)

--_uncurry::(a->(b->c))->(a,b)->c
_uncurry::Num a=>(a->(a->a))->(a,a)->a
_uncurry f (x,y) = f x y

smaller,bigger::Int->Int->Bool
smaller e f = if e<f then True else False
bigger e f = if f<e then True else False

myex2::[Int]->Int->(Int->Int->Bool)->Int
myex2 (h:t) cmp f
 | t==[] = if (f h cmp) then h else cmp
 | h<cmp = myex2 t h f
 | otherwise = myex2 t cmp f

extremum::[Int]->(Int->Int->Bool)->Int
extremum (h:t) f = if t==[] then h else myex2 t h f

_filter::(a->Bool)->[a]->[a]
_filter p [] = []
_filter p (x:xs) 
 | (p x)==True = x: _filter p xs
 | otherwise = _filter p xs

_map::(a->b)->[a]->[b]
_map f [] = []
_map f (x:xs) = (f x): _map f xs


------- using (a private version of) foldr
_sum::Num a=>[a]->a
_sum []     = 0
_sum (x:xs) = x+_sum xs

_product::Num a=>[a]->a
_product []     = 1
_product (x:xs) = x*_product xs

_and::[Bool]->Bool
_and []     = True
_and (x:xs) = x&&_and xs

_maximum::[Int]->Int
_maximum (x:xs) = foldr (max) x xs

-- call: _foldr (+) 0 [1,3] => 4
--_foldr::(a->a->a)->a->[a]->a, too "specifc"
_foldr::(a->b->b)->b->[a]->b
_foldr f e []     = e
_foldr f e (x:xs) = f x (_foldr f e xs)

_foldl::(b->a->b)->b->[a]->b
_foldl f e []     = e
_foldl f e (x:xs) = f (_foldl f e xs) x

-- decimal transforms [1,2,3] into 123
--decimal::
decimal [] = 0
decimal (x:xs) = x*10^(length xs) + decimal xs

-- _decimal [1,2,3] (+) 10 => 123
_decimal [] f basis = 0
_decimal (x:xs) f basis = f (x*basis^(length xs)) (_decimal xs f basis)

_until,_while::(a->Bool)->(a->a)->a->a
_until p f x | p x = x
             | otherwise = _until p f (f x)

_while p f x
 | (p x)==False = x
 | otherwise = _while p f (f x)

divisable::Int->Int->Bool
divisable m n = if (m `mod` n ==0) then True else False

denominators::Int->[Int]
denominators x = filter (divisable x) [1..x]

prime::Int->Bool
prime x = denominators x == [1,x]

primes::Int->[Int]
primes x = filter prime [1..x]

-- InsertSort
insert::Ord a=>a->[a]->[a]
insert e (x:xs)
 | e<=x = e:(x:xs)
 | otherwise = x:(insert e xs)

-- QuickSort
qsort::[Int]->[Int]
qsort [] = []
qsort (x:xs) = qsort(filter (smaller x) xs) ++ [x] ++ qsort(filter (bigger x) xs)

-- BubbleSort
bsort::Ord a=>[a]->[a]
bsort [] = []
bsort l = max : (bsort (filter (<max) l)) where max=maximum l


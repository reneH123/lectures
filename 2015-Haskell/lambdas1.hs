module Lambdas where

square::Int->Int
square x = x*x

-- Combinator Logic (aka Schoenfinkel's term combinators, mathematician and logican from Saint Petersburg, Russia)

i=(\x -> x)
k=(\x -> \y -> x)
s=(\x y z -> (x z (y z)))


{--  k = [x1,x2,....,xn]
     inits k => [[],[x1],[x1,x2],[x1,x2,x3],...[x1,x2,...,xn]]
 --}

-- copyToN [11,42,33,14,5] 3 => [11,42,33]
copyToN::[a]->Int->[a]
copyToN [] _ = []
copyToN _ 0 = []
copyToN (x:xs) n = x:(copyToN xs (n-1))

-- inits2 [11,42,33] 3 => [[],[11],[11,42],[11,42,33]]
inits2::[a]->Int->[[a]]
inits2 _ 0 = [[]]
inits2 l k = (copyToN l k):(inits2 l (k-1))

-- inits [11,42,33] => [[],[11],[11,42],[11,42,33]]
inits::[a]->[[a]]
inits l = reverse(inits2 l (length l))

-- tails [11,42,33] => [[11,42,33],[42,33],[33],[]]
tails::[a]->[[a]]
tails l = map (reverse) (inits2 (reverse l) (length l))

-- segments [1,2,3] => [[1],[2],[3],[1,2],[2,3],[1,2,3]]
segs2::[a]->[[a]]
segs2 [] = [[]]
segs2 l = (inits l)++(segs2 (tail l))

segments l = filter (\x->x/=[]) (segs2 l)

{-- fib, fib2
    optimised Fibonacci numbers after one unfold/fold
 --}

fib n = x+y where (x,y)=fib2 (n-1)
fib2 0 = (1,1)
fib2 n = (x+y,x) where (x,y)=fib2 (n-1)

{-- trans  ... transitive closure
    lexOrd ... lexico-graphical ordered pairs
 --}
 
type CP = [(Int,Int)]
--trans::CP->CP
trans l = [(a,c)|(a,b)<-l,(b,c)<-l]
trans l = l++[x|x<-ext,(not (elem x l))]
					where ext=[(a,c)|(a,b)<-l,(b2,c)<-l,b==b2]
					
qsort::Ord a=>[a]->[a]
qsort [] = []
qsort (x:xs) = qsort [y|y<-xs,y<x] ++ [x] ++ qsort [y|y<-xs,x<y]

{-- quickSort extended, so it works with tuples --}
lexOrd::CP->CP
lexOrd [] = []
lexOrd ((x,x2):xs) = lexOrd [(y,y2)|(y,y2)<-xs,((y<x)||((y==x)&&(y2<x2)))] 
  										++ [(x,x2)] 
  										++ lexOrd [(y,y2)|(y,y2)<-xs,((x<y)||((x==y)&&(x2<y2)))]
  										
{-- perms [1,2,3] => [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]] --}
perms [] = [[]]
perms l = [(a:b)| a<-l, b<-perms (filter (/=a) l)]

{-- duplicates ... duplicates by list abstraction
    duplicates2 ... duplicates by fold-map
 --}

duplicates::[String]->[String]
duplicates [] = []
duplicates (x:xs) = [w|w<-xs,w==x]++(duplicates xs)

dup2 l = foldr (\x y->if (sum (map (\x2->if (x2==x) then 1 else 0) l)>1) then (y++[x]) else y) [] l
duplicates2 l = (qsort.dup2) l

{-- determine all (sub-)sums of all numbers --}
 
aux1 _ 0 = []
aux1 totalSum z
 | (totalSum>=z) = z:aux1 (totalSum-z) z -- while totalSum is not reached -> continue with partitioning
 | True = aux1 totalSum (z-1)
 
aux2 _ 0 = []
aux2 totalSum z
 | (totalSum>=z) = z:aux2 (totalSum-z) z -- ditto
 | True = aux2 totalSum (z-1)

sums2 n = foldr (\x y->([aux2 n x]++y)) [] [1..n]

-- sums 3 => [[1,1,1],[1,2],[2,1],[3]]
--nth::Int->[a]->a
proj 0 [] = -1
proj 0 (x:xs) = x
proj _ [] = -1
proj n (x:xs) = proj (n-1) xs

xproj 0 [] = []
xproj 0 (x:xs) = xs
xproj n (x:xs) = x:(xproj (n-1) xs)

{--merge::[Int]->Int->[Int]
merge     []   _ = []
merge (x:xs) pos = let y=(proj pos xs)
                       xs'=xproj pos xs
                   in (x+y):(merge xs' pos) --}
                   
merge2::[Int]->[[Int]]
merge2 [] = [[]]
merge2 [x] = [[x]]
merge2 (x:(y:ys)) = [if (flag==True) then ((x+y):m) else (x:m)| flag<-[True,False],m<-(merge2 (y:ys))] 

-- [1,2,3,5] 6 => 6, because of [1,2,3]
subsumcontains [] 0 res = []
subsumcontains (x:xs) s res
 | (sumres==s)   = res 
 | (sumres+x==s) = res++[x]
 | (sumres+x>s)  = []
 | True          = subsumcontains xs s (res++[x])
 where sumres = sum res

sums3 n = (reverse.noEmpty) noDuplicates
           where ones = map (\x->1) [1..n]
                 allsums = merge2 ones
                 subsums = map (\x->subsumcontains x n []) allsums
                 noEmpty = filter (\x->x/=[])
                 noDuplicates = foldr (\x y->if (elem x y) then y else x:y) [[]] (noEmpty subsums)
                 
{-- allsums merges until T(n+1)=T(n), otherwise continue with merging sums --}


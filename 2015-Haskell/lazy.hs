-- infinity doesn't terminate
-- square is a strict function
--   square infinity doesn't terminate
-- three is not
--   three infinity terminates
infinity = infinity + 1
three x = 3

square::Integer->Integer
square x = x*x

squareNew::Integer->Integer
squareNew (x) = x*x

-- call :> const error("terminate!") will not terminate
const::a->Int
const x = 1

e::Float
e = 2.781

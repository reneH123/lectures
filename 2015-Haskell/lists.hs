--mymap::[a]->Int->a
mymap l n
 | n==0 = head l
 | otherwise = mymap (tail l) (n-1)

--myappend::[a]->[a]->[a]
myappend l1 l2
 | l1==[] = l2
 | otherwise = (head l1):(myappend (tail l1) l2)

--concat2::[[a]]->[a]->[a]
concat2 l li
 | l==[] = li
 | otherwise = concat2 (tail l) (myappend li (head l))

--myconcat::[[a]]->[a]
myconcat l
 = concat2 l []

--myreverse::[Int]->[Int]
myreverse l
 | l==[] = []
 | otherwise = myappend (myreverse (tail l)) ((head l):[])
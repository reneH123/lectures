{-- Task 1
    sums [1,0,3,4,-2] => [0,1,1,4,8,6]
 --}
 
inits (a:as) = foldl (\y x-> (y++[((last y)++[x])]) ) [[a]] as
sums l = 0:map (\x->sum x) (inits l)

{-- Task 2
     redefinition of Haskell lists
 --}
 
data List a = Empty |
          Next a (List a) deriving Show

cons::a->List a->List a
cons a l = (Next a l)

-- my_take::Int->List a->List a
my_take 0 l = Empty
my_take n Empty = Empty
my_take n (Next a l) = Next a (my_take (n-1) (l))

class Sequence a where
 s_take::Int->a->a
 
instance Sequence [a] where
 s_take = take
 
instance Sequence (List a) where
 s_take = my_take 
 
 {-- Task 3:
  define putStr by using monads
  --}
  
myPutStr::String->IO ()
myPutStr [] = return ()
{--myPutStr (c:cs) = do 
										 putChar c
										 myPutStr cs--}
myPutStr (c:cs) = putChar c >>= (\_->myPutStr cs)

{-- Task 7: 
 x+0 -> x
 simplifying expressions
 --}
ununit::Maybe a->a
ununit (Just a)=a 
 
data Expr = Const Integer | Var String | Plus Expr Expr | Minus Expr Expr |
            Times Expr Expr | Div Expr Expr deriving (Eq,Show)
            
simplify::Expr->Maybe Expr
simplify (Plus (Const 0) x) = simplify x
simplify (Plus x (Const 0)) = simplify x
simplify (Minus x (Const 0)) = simplify x
simplify (Times (Const 0) x) = Just (Const 0)
simplify (Times x (Const 0)) = Just (Const 0)
simplify (Plus x y) = Just (Plus (ununit (simplify x)) (ununit (simplify y)))
simplify (Minus (Const 0) x) = Just (Minus (Const 0) (ununit (simplify x)))
simplify (Minus x y) = Just (Minus (ununit (simplify x)) (ununit (simplify y)))
simplify (Times x y) = Just (Times (ununit (simplify x)) (ununit (simplify y)))
simplify (Div x (Const 0)) = Nothing
simplify (Div x y) = Just (Div (ununit (simplify x)) (ununit (simplify y)))
simplify e = Just e


data M a = Raise Exception | Return a

type Exception = String


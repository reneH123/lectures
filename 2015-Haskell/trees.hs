data Tree a = Node a (Tree a) (Tree a) | Leaf
data TwoThreeTree a = NodeTwo a (TwoThreeTree a) (TwoThreeTree a) |
	              NodeThree a (TwoThreeTree a) (TwoThreeTree a) (TwoThreeTree a) |
		      LeafTwoThree

-- check if is member ?
elemTree::Ord a=>a->Tree a->Bool
elemTree e Leaf = False
elemTree e (Node x l r) 
 | e==x = True
 | e<x  = elemTree e l
 | e>x  = elemTree e r

-- height
heightTree::Tree a->Int
heightTree Leaf = 0
heightTree (Node x l r) = 1+ (max (heightTree l) (heightTree r))

-- sum
totalTree::Num a=>Tree a->a
totalTree Leaf = 0
totalTree (Node x l r) = x + (totalTree l) + (totalTree r)

-- tree --> expression
expression::Tree [Char]->[Char]
expression Leaf         = ""
expression (Node a l r) = unwords[expression l,a,expression r]

--call:
--  expression (Node ":=" (Node "a" (Leaf) (Leaf)) (Node "+" (Node "b" (Leaf)(
--              Leaf))(Node "1" (Leaf)(Leaf))))
-- implies "a := b + 1 "
